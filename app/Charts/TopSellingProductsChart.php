<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;

class TopSellingProductsChart extends Chart {
    public function __construct($arrProducts) {
        parent::__construct();

        $labels = [];
        $data = [];

        foreach ($arrProducts as $objProduct) {
            $labels[] = $objProduct->formattedName;
        }

        foreach ($arrProducts as $objProduct) {
            $data[] = $objProduct->sum;
        }

        $this->displayAxes(false);
        $this->displayLegend(false);
        $this->labels($labels);
        $this->dataset('Top selling products', 'pie', $data)->backgroundColor(['#EA7224', '#7A9E9F', '#B8D8D8']);
    }
}
