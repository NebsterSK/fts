<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model {
    protected $fillable = ['name'];

    public function getRouteKeyName() {
        return 'name';
    }

    // Relations
    public function ordered_product() {
        return $this->hasOne(OrderedProduct::class);
    }
}
