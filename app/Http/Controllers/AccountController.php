<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AccountRequest;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\AccountDeleteRequest;
use App\User;

class AccountController extends Controller {
    public function index() {
        return view('account/index');
    }

    public function update(AccountRequest $objRequest) {
        $arrData['name'] = $objRequest->name;
        $arrData['surname'] = $objRequest->surname;

        if ($objRequest->password !== null) $arrData['password'] = Hash::make($objRequest->password);

        Auth::user()->update($arrData);

        Alert::toast('Account settings saved.', 'success');

        return back();
    }

    public function download() {
        $strData = Auth::user()->makeHidden(['admin'])->load('orders.ordered_products');

        Storage::put('public/user_data/' . Auth::user()->id . '.txt', $strData);

        return response()->download(storage_path('app/public/user_data/' . Auth::user()->id . '.txt'));
    }

    public function delete(AccountDeleteRequest $objRequest) {
        $intUserId = Auth::user()->id;
        Auth::logout();
        User::destroy($intUserId);

        Alert::toast('Your account has been deleted.', 'success');

        return redirect(route('index'));
    }
}
