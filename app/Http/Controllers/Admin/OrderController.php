<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\Http\Requests\OrderStatusRequest;

class OrderController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $arrOrders = Order::orderBy('id', 'DESC')->paginate(50);

        return view('admin/orders/index')->with([
            'arrOrders' => $arrOrders
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    public function show(Order $order) {
        $order->load('comments');

        return view('admin/orders/show')->with([
            'objOrder' => $order
        ]);
    }

    public function invoice(Order $order) {
        return response()->download(storage_path('app/public/invoices/' . $order->id . '.pdf'));
    }

    public function status(Order $order, OrderStatusRequest $objRequest) {
        $order->status = $objRequest->status;
        $order->save();

        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
