<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AccountRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $arrUsers = User::orderBy('id', 'DESC')->paginate(50);

        return view('admin/users/index')->with([
            'arrUsers' => $arrUsers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user) {
        $user->load(['orders', 'comments']);

        return view('admin/users/show')->with([
            'objUser' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user) {
        return view('admin/users/edit')->with([
            'objUser' => $user
        ]);
    }

    public function update(AccountRequest $objRequest, $id) {
        $arrData['name'] = $objRequest->name;
        $arrData['surname'] = $objRequest->surname;

        if ($objRequest->password !== null) $arrData['password'] = Hash::make($objRequest->password);

        Auth::user()->update($arrData);

        Alert::toast('Account settings saved.', 'success');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
