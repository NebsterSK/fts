<?php

namespace App\Http\Controllers;

use App\OrderedProduct;
use Illuminate\Http\Request;
use App\Order;
use App\User;
use DB;
use App\Charts\TopSellingProductsChart;

class AdminController extends Controller {
    public function index() {
        // Newest orders
        $arrOrders = Order::orderBy('id', 'DESC')->limit(10)->get();

        // Newest users
        $arrUsers = User::orderBy('id', 'DESC')->limit(10)->get();

        // Top selling products
        $arrProducts = OrderedProduct::select('product_id', 'color_id', 'size_id')
            ->addSelect(DB::raw('SUM(amount) AS sum'))
            ->addSelect(DB::raw('SUM(amount) / (SELECT SUM(amount) FROM ordered_products) * 100 AS perc'))
            ->groupBy('product_id', 'color_id', 'size_id')
            ->orderBy('sum', 'DESC')
            ->limit(5)
            ->get();

        $objChart = new TopSellingProductsChart($arrProducts);

        return view('admin/index')->with([
            'arrOrders' => $arrOrders,
            'arrUsers' => $arrUsers,
            'arrProducts' => $arrProducts,
            'objChart' => $objChart
        ]);
    }
}
