<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserAuthenticated;
use App\Events\UserCreated;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Socialite;
use App\User;
use Auth;

class LoginController extends Controller {
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request, $user) {
        event(new UserAuthenticated());
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider() {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback() {
        $socialUser = Socialite::driver('facebook')->user();

        $user = User::where(['email' => $socialUser->getEmail()])->first();

        // Register User if doesnt exist
        if (!$user) {
            $user = User::create([
                'name' => explode(' ', $socialUser->getName())[0],
                'surname' => explode(' ', $socialUser->getName())[1] ?? null,
                'email' => $socialUser->getEmail(),
            ]);

            event(new UserCreated());
        }

        Auth::login($user);

        return redirect(route('index'));
    }
}
