<?php

namespace App\Http\Controllers;

use App\Events\CartAdded;
use App\Events\CartRemoved;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Requests\CartAddRequest;
use RealRashid\SweetAlert\Facades\Alert;
use App\Product;

class CartController extends Controller {
    public function index() {
        return view('cart')->with([
            'objCart' => Cart::content()
        ]);
    }

    public function add(CartAddRequest $objRequest) {
        $objProduct = Product::find($objRequest->id);

        Cart::add($objRequest->id, $objProduct->name, $objRequest->quantity, $objProduct->price, 175, [
            'color' => $objRequest->color,
            'size' => $objRequest->size
        ]);

        event(new CartAdded());

        Alert::toast(__('cart.added'), 'success');

        return back();
    }

    public function remove($id) {
        Cart::remove($id);

        event(new CartRemoved());

        return back();
    }
}
