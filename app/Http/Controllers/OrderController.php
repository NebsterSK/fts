<?php

namespace App\Http\Controllers;

use App\Events\OrderCreated;
use App\Events\OrderPaid;
use ConsoleTVs\Invoices\Classes\Invoice;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Http\Requests\OrderRequest;
use App\Order;
use App\OrderedProduct;
use Auth;
use App\Classes\PayPalClient;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;
use RealRashid\SweetAlert\Facades\Alert;
use DB;

class OrderController extends Controller {
    public function index() {
        $arrOrders = Order::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->get();

        return view('account/orders/index')->with([
            'arrOrders' => $arrOrders
        ]);
    }

    public function show(Order $order) {
        if (Auth::user()->id != $order->user_id) abort(401);

        $order->load('ordered_products');

        return view('account/orders/show')->with([
            'objOrder' => $order
        ]);
    }

    public function create() {
        return view('orders/create')->with([
            'objCart' => Cart::content()
        ]);
    }

    public function store(OrderRequest $objRequest) {
        $arrOrder = $objRequest->all() + ['total' => Cart::subtotalFloat() + 1];

        if (Auth::check()) $arrOrder['user_id'] = Auth::user()->id;

        $objOrder = DB::transaction(function() use ($arrOrder) {
            $objOrder = Order::create($arrOrder);

            foreach(Cart::content() as $objItem) {
                if ($objItem->options->color == 'white') $color = 1;
                elseif ($objItem->options->color == 'black') $color = 2;
                else $color = null;

                if ($objItem->options->size == 's') $size = 1;
                elseif ($objItem->options->size == 'm') $size = 2;
                else $size = 3;

                OrderedProduct::create([
                    'order_id' => $objOrder->id,
                    'product_id' => $objItem->id,
                    'color_id' => $color,
                    'size_id' => $size,
                    'amount' => $objItem->qty
                ]);
            }

            return $objOrder;
        }, 3);

        $objOrder->load('ordered_products');

        event(new OrderCreated($objOrder));

        Cart::destroy();

        return redirect(route('orders.payment', ['id' => $objOrder->id]));
    }

    public function payment(Order $order) {
        $order->load('ordered_products');

        return view('orders/payment')->with([
            'objOrder' => $order
        ]);
    }

    public function pay(Order $order, $paypal_id) {
        $objOrder = $order->load('ordered_products');
        $response = PayPalClient::client()->execute(new OrdersGetRequest($paypal_id));

        if ($response->result->status != "COMPLETED") {
            Alert::error('Something went wrong!', 'Your payment could not be processed.');

            return back();
        }

        $objOrder->pay($paypal_id);
        $objOrder->save();

        event(new OrderPaid($objOrder));

        Alert::success('Success!', 'Your order was successfully paid.');

        return Auth::check() ? redirect(route('account.orders.show', ['id' => $objOrder->id])) : redirect(route('index'));
    }

    public function invoice(Order $order) {
        if (Auth::user()->id != $order->user_id) abort(401);

        return response()->download(storage_path('app/public/invoices/' . $order->id . '.pdf'));
    }

    public function generateInvoice(Order $order) {
        $objInvoice = Invoice::make();

        foreach ($order->ordered_products as $objProduct) {
            $objInvoice->addItem($objProduct->formattedName, round($objProduct->product->price * 0.8, 2), $objProduct->amount);
        }

        return $objInvoice->addItem('Shipping', 0.8, 1)
            ->number('FTS' . $order->id)
            ->tax($order->total - $objInvoice->subTotalPriceFormatted())
            ->customer([
                'name'      => $order->fullName,
                'location'  => $order->street,
                'zip'       => $order->zip,
                'city'      => $order->city,
                'country'   => $order->country,
            ])
            ->show('public/invoices/'. $order->id . '.pdf');
    }
}
