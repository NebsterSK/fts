<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Color;
use App\Size;

class ProductController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('products/index');
    }

    public function old($color = 'black', $size = 'm') {
        return view('products/old')->with([
            'color' => $color,
            'size' => $size
        ]);
    }

    public function sober($color = null, $size = null) {
        return view('products/sober')->with([
            'color' => $color,
            'size' => $size
        ]);
    }

    public function set($size = null) {
        return view('products/set')->with([
            'size' => $size
        ]);
    }

//    /**
//     * Show the form for creating a new resource.
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function create() {
//        //
//    }
//
//    /**
//     * Store a newly created resource in storage.
//     *
//     * @param \Illuminate\Http\Request $request
//     * @return \Illuminate\Http\Response
//     */
//    public function store(Request $request) {
//        //
//    }

    public function show(Product $product, Color $color, Size $size) {
        return view('products/show')->with([
            'objProduct' => $product,
            'color' => $color,
            'size' => $size
        ]);
    }

//    /**
//     * Show the form for editing the specified resource.
//     *
//     * @param int $id
//     * @return \Illuminate\Http\Response
//     */
//    public function edit($id) {
//        //
//    }
//
//    /**
//     * Update the specified resource in storage.
//     *
//     * @param \Illuminate\Http\Request $request
//     * @param int $id
//     * @return \Illuminate\Http\Response
//     */
//    public function update(Request $request, $id) {
//        //
//    }
//
//    /**
//     * Remove the specified resource from storage.
//     *
//     * @param int $id
//     * @return \Illuminate\Http\Response
//     */
//    public function destroy($id) {
//        //
//    }
}
