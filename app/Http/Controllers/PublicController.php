<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller {
    public function index() {
        return view('index');
    }

    public function brand() {
        return view('brand');
    }

    public function contact() {
        return view('contact');
    }

    public function faq() {
        return view('faq');
    }

    public function pp() {
        return view('pp');
    }

    public function tac() {
        return view('tac');
    }

    public function car() {
        return view('car');
    }
}
