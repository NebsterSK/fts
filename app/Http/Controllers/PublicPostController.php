<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Jobs\SendContactMailJob;
use RealRashid\SweetAlert\Facades\Alert;

class PublicPostController extends Controller {
    public function contact(ContactRequest $objRequest) {
        SendContactMailJob::dispatch($objRequest->all());

        Alert::success('Success!', 'Your message was sent successfully. We will contact you as soon as possible.')
            ->persistent()
            ->showConfirmButton('OK');

        return back();
    }
}
