<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Config;
use App;

class Localization {
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (isset($_GET['lang']) and in_array($_GET['lang'], config('app.allowed_locales'))) Session::put('appLocale', $_GET['lang']);

        if (Session::has('appLocale')) $lang = Session::get('appLocale');
        else $lang = Config::get('app.fallback_locale');

        App::setLocale($lang);

        return $next($request);
    }
}
