<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CartAddRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'id' => 'integer|required|exists:products,id',
            'quantity' => 'integer|required|min:1',
            'size' => [
                'required',
                'string',
                Rule::in(['s', 'm', 'l']),
            ],
            'color' => [
                'string',
                Rule::in(['white', 'black']),
            ],
        ];
    }
}
