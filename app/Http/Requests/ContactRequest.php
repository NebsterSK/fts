<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $arrRules = [
            'name' => 'required|string|max:100',
            'email' => 'required|string|max:100',
            'message' => 'required|string|max:300'
        ];

        if (config('app.env') != 'testing') $arrRules['g-recaptcha-response'] = 'recaptcha';

        return $arrRules;
    }
}
