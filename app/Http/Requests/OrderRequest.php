<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'string|required|max:20',
            'surname' => 'string|required|max:30',
            'email' => 'email|required|max:100',
            'phone' => 'string|required|max:30',
            'street' => 'string|required|max:100',
            'city' => 'string|required|max:50',
            'zip' => 'string|required|max:20',
            'country' => 'string|required|max:50',
            'note' => 'string|max:300',
            'tac' => 'accepted',
        ];
    }
}
