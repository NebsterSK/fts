<?php

namespace App\Jobs;

use App\Order;
use ConsoleTVs\Invoices\Classes\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GenerateOrderInvoiceJob implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $objOrder;

    public function __construct(Order $objOrder) {
        $this->objOrder = $objOrder;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $objInvoice = Invoice::make();

        foreach ($this->objOrder->ordered_products as $objProduct) {
            $objInvoice->addItem($objProduct->formattedName, round($objProduct->product->price * 0.8, 2), $objProduct->amount);
        }

        $objInvoice->addItem('Shipping', 0.8, 1)
            ->number('FTS' . $this->objOrder->id)
            ->tax($this->objOrder->total - $objInvoice->subTotalPriceFormatted())
            ->customer([
                'name'      => $this->objOrder->fullName,
                'location'  => $this->objOrder->street,
                'zip'       => $this->objOrder->zip,
                'city'      => $this->objOrder->city,
                'country'   => $this->objOrder->country,
            ])
            ->save('public/invoices/'. $this->objOrder->id . '.pdf');
    }
}
