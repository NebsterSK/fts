<?php

namespace App\Jobs;

use App\Mail\OrderCreatedMail;
use App\Mail\NewOrderMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Order;
use GAMP;

class OrderCreatedJob implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $objOrder;

    public function __construct(Order $objOrder) {
        $this->objOrder = $objOrder;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        Mail::to($this->objOrder->email)->send(new OrderCreatedMail($this->objOrder));
        Mail::to('fts@yasmin-trade.com')->send(new NewOrderMail($this->objOrder));

        GAMP::setClientId('123456')
            ->setEventCategory('Order')
            ->setEventAction('Order Creation')
            ->setEventLabel('User created an Order')
            ->sendEvent();
    }
}
