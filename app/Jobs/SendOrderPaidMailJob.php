<?php

namespace App\Jobs;

use App\Mail\OrderPaidMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Order;

class SendOrderPaidMailJob implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $objOrder;

    public function __construct(Order $objOrder) {
        $this->objOrder = $objOrder;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        Mail::to($this->objOrder->email)->send(new OrderPaidMail($this->objOrder));
    }
}
