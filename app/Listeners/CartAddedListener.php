<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\CartAdded;
use GAMP;

class CartAddedListener {
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(CartAdded $event) {
        GAMP::setClientId('123456')
            ->setEventCategory('Cart')
            ->setEventAction('Cart item Addition')
            ->setEventLabel('User added an item to Cart')
            ->sendEvent();
    }
}
