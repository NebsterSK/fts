<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\CartRemoved;
use GAMP;

class CartRemovedListener {
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(CartRemoved $event) {
        GAMP::setClientId('123456')
            ->setEventCategory('Cart')
            ->setEventAction('Cart item Removal')
            ->setEventLabel('User removed an item from Cart')
            ->sendEvent();
    }
}
