<?php

namespace App\Listeners;

use App\Jobs\OrderCreatedJob;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\OrderCreated;

class OrderCreatedListener {
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(OrderCreated $event) {
        OrderCreatedJob::dispatch($event->objOrder);
    }
}
