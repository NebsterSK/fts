<?php

namespace App\Listeners;

use App\Jobs\GenerateOrderInvoiceJob;
use App\Jobs\SendOrderPaidMailJob;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\OrderPaid;
use GAMP;

class OrderPaidListener {
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(OrderPaid $event) {
        GenerateOrderInvoiceJob::withChain([
            new SendOrderPaidMailJob($event->objOrder)
        ])->dispatch($event->objOrder);

        GAMP::setClientId('123456')
            ->setEventCategory('Order')
            ->setEventAction('Order Payment')
            ->setEventLabel('User paid for an Order')
            ->sendEvent();
    }
}
