<?php

namespace App\Listeners;

use App\Events\UserAuthenticated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use GAMP;

class UserAuthenticatedListener {
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(UserAuthenticated $event) {
        GAMP::setClientId('123456')
            ->setEventCategory('User')
            ->setEventAction('User Log in')
            ->setEventLabel('User logged in')
            ->sendEvent();
    }
}
