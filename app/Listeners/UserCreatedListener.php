<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserCreated;
use GAMP;

class UserCreatedListener {
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(UserCreated $event) {
        GAMP::setClientId('123456')
            ->setEventCategory('User')
            ->setEventAction('User Registration')
            ->setEventLabel('User registered an Account')
            ->sendEvent();
    }
}
