<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable {
    use Queueable, SerializesModels;

    public $arrData;

    public function __construct($arrData) {
        $this->arrData = $arrData;
    }

    public function build() {
        return $this->from($this->arrData['email'])
            ->replyTo($this->arrData['email'])
            ->subject('Contact form')
            ->markdown('emails/contact')
            ->with('arrData', $this->arrData);
    }
}
