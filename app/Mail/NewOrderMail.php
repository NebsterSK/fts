<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Order;

class NewOrderMail extends Mailable {
    use Queueable, SerializesModels;

    public $objOrder;

    public function __construct(Order $objOrder) {
        $this->objOrder = $objOrder;
    }

    public function build() {
        return $this->from('fts@yasmin-trade.com')
            ->replyTo('fts@yasmin-trade.com')
            ->subject('Order #' . $this->objOrder->id)
            ->markdown('emails/orders/new');
    }
}
