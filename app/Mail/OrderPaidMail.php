<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Order;

class OrderPaidMail extends Mailable {
    use Queueable, SerializesModels;

    public $objOrder;

    public function __construct(Order $objOrder) {
        $this->objOrder = $objOrder;
    }

    public function build() {
        return $this->from('fts@yasmin-trade.com')
            ->replyTo('fts@yasmin-trade.com')
            ->subject('For This Shi(r)t order paid')
            ->markdown('emails/orders/paid')
            ->attachFromStorage('public/invoices/' . $this->objOrder->id . '.pdf');
    }
}
