<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {
    protected $fillable = ['user_id', 'name', 'surname', 'email', 'phone', 'street', 'city', 'zip', 'country', 'note', 'total',
        'paid', 'paypal_order_id', 'paid_at'];

    protected $casts = [
        'total' => 'float',
        'paid' => 'boolean',
        'paid_at' => 'datetime'
    ];

    public function pay($strPayPalOrderId) {
        $this->status = 'paid';
        $this->paid = 1;
        $this->paypal_order_id = $strPayPalOrderId;
        $this->paid_at = date('Y-m-d H:i:s');
    }

    // Accessors
    public function getFullNameAttribute() {
        return $this->name . ' ' . $this->surname;
    }

    public function getAddressAttribute() {
        return $this->street . ', ' . $this->city . ', ' . $this->zip . ', ' . $this->country;
    }

    // Relations
    public function ordered_products() {
        return $this->hasMany(OrderedProduct::class);
    }

    public function comments() {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
