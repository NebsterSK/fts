<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderedProduct extends Model {
    protected $fillable = ['order_id', 'product_id', 'color_id', 'size_id', 'amount'];

    protected $with = ['product', 'color', 'size'];

    // Accessors
    public function getFormattedNameAttribute() {
        $str = $this->product->name;
        if ($this->color != null) $str .= ', ' . ucfirst($this->colorName);
        return $str .= ', ' . ucfirst($this->size->name);
    }

    public function getColorNameAttribute() {
        return $this->color != null ? $this->color->name : '';
    }

    // Relations
    public function order() {
        return $this->belongsTo(Order::class);
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function color() {
        return $this->belongsTo(Color::class);
    }

    public function size() {
        return $this->belongsTo(Size::class);
    }
}
