<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {
    protected $fillable = ['name', 'slug', 'description', 'price'];

    protected $casts = [
        'price' => 'float'
    ];

    public function getRouteKeyName() {
        return 'slug';
    }

    // Relations
    public function ordered_products() {
        return $this->hasMany(OrderedProduct::class);
    }
}
