<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'admin' => 'boolean'
    ];

    // Accessors
    public function getFullNameAttribute() {
        return $this->name . ' ' . $this->surname;
    }

    // Scopes
    public function scopeAdmin($query) {
        return $query->where('admin', 1);
    }

    // Relations
    public function orders() {
        return $this->hasMany(Order::class);
    }

    public function author_of() {
        return $this->hasMany(Comment::class, 'author_id');
    }

    public function comments() {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
