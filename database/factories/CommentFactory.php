<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;
use App\User;
use App\Order;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'author_id' => User::admin()->get()->random()->id,
        'commentable_id' => null,
        'commentable_type' => '',
        'text' => $faker->text(200)
    ];
});

// States
$factory->state(Comment::class,'user', function (Faker $faker) {
    return [
        'commentable_id' => User::all()->random()->id,
        'commentable_type' => 'App\User'
    ];
});

$factory->state(Comment::class,'order', function (Faker $faker) {
    return [
        'commentable_id' => Order::all()->random()->id,
        'commentable_type' => 'App\Order'
    ];
});
