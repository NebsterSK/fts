<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use App\Order;
use App\User;
use App\OrderedProduct;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'user_id' => null,
        'status' => 'created',
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'email' => $faker->safeEmail,
        'phone' => $faker->phoneNumber,
        'street' => $faker->streetAddress,
        'city' => $faker->city,
        'zip' => $faker->postcode,
        'country' => $faker->country,
        'note' => $faker->text(100),
        'total' => 15,
        'paid' => null,
        'paypal_order_id' => null,
        'paid_at' => null
    ];
});

// States
$factory->state(Order::class, 'registered', function ($faker) {
    $objUser = User::all()->random();

    return [
        'user_id' => $objUser->id,
        'name' => $objUser->name,
        'surname' => $objUser->surname,
        'email' => $objUser->email
    ];
});

$factory->state(Order::class, 'registered_random', function ($faker) {
    if ((bool)random_int(0, 1)) {
        $objUser = User::all()->random();

        return [
            'user_id' => $objUser->id,
            'name' => $objUser->name,
            'surname' => $objUser->surname,
            'email' => $objUser->email
        ];
    }

    return [];
});

$factory->state(Order::class, 'paid', function ($faker) {
    return [
        'status' => 'paid',
        'paid' => 1,
        'paypal_order_id' => 'O2fb91iul35x',
        'paid_at' => now()
    ];
});

$factory->state(Order::class, 'paid_random', function ($faker) {
    if ((bool)random_int(0, 1)) {
        return [
            'status' => 'paid',
            'paid' => 1,
            'paypal_order_id' => 'O2fb91iul35x',
            'paid_at' => now()
        ];
    }

    return [];
});

// Callbacks
$factory->afterCreating(Order::class, function ($order, $faker) {
    $intPrice = 2;

    for ($i = 1; $i <= random_int(1, 3); $i++) {
        $order->ordered_products()->save($objOrderedProduct = factory(OrderedProduct::class)->state('set_random')->make());

        $intPrice += $objOrderedProduct->product->price * $objOrderedProduct->amount;
    }

    $order->total = $intPrice;
    $order->save();
});

$factory->afterCreating(Order::class, function ($user, $faker) {
    for ($i = 1; $i <= random_int(0, 3); $i++) {
        $user->comments()->save(factory(Comment::class)->state('order')->make());
    }
});
