<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OrderedProduct;
use App\Product;
use App\Color;
use App\Size;
use Faker\Generator as Faker;

$factory->define(OrderedProduct::class, function (Faker $faker) {
    return [
        'product_id' => Product::all()->random()->id,
        'color_id' => Color::all()->random()->id,
        'size_id' => Size::all()->random()->id,
        'amount' => random_int(1, 3)
    ];
});

// States
$factory->state(OrderedProduct::class, 'set_random', function ($faker) {
    $intProductId = Product::all()->random()->id;

    if ($intProductId == 3) {
        $array = [
            'product_id' => 3,
            'color_id' => null,
        ];
    }
    else {
        $array = [];
    }

    return $array;
});

$factory->state(OrderedProduct::class, 'set', function ($faker) {
    return [
        'product_id' => 3,
        'color_id' => null,
    ];
});
