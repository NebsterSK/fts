<?php

use Illuminate\Database\Seeder;
use App\User;

class AdminsSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        User::create([
            'name' => 'Lukas',
            'surname' => 'FTS',
            'email' => 'lukas@yasmin-trade.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'admin' => true
        ]);

        User::create([
            'name' => 'Vlado',
            'surname' => 'FTS',
            'email' => 'vlado@yasmin-trade.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'admin' => true
        ]);
    }
}
