<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Product::create([
            'name' => 'T-shirt OLD',
            'slug' => 'old',
            'description' => 'description',
            'price' => 13
        ]);

        Product::create([
            'name' => 'T-shirt SOBER',
            'slug' => 'sober',
            'description' => 'description',
            'price' => 13
        ]);
    }
}
