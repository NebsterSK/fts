<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'logIn' => 'Log in',
    'or' => 'OR',
    'email' => 'E-mail',
    'password' => 'Password',
    'remember' => 'Remember me',
    'forgot' => 'Forgot your password?',

    'register' => 'Register',
    'name' => 'Name',
    'passwordConfirm' => 'Confirm password',
    'read' => 'I have read and understand',

    'fname' => 'First name',
    'lname' => 'Last name',
    'street' => 'Street',
    'city' => 'City',
    'zip' => 'ZIP code',
    'country' => 'Country',
    'phone' => 'Phone',
    'note' => 'Note',

];
