<?php

return [

    'title' => 'Brand',
    1 => 'brand was founded in 2019 by trading company Yasmin Trade s.r.o. to sell non-formal clothing.',
    2 => 'T-shirts are made of 100% cotton to ensure high quality, long durability and comfort. They are manufactured using "tunnel sewing", which means <u>there are no joints on the sides</u>.',
    3 => 'We offer shipping via post office mainly in <strong>Central Europe</strong>. If you are from a far but really interested in our products contact us and we sure can make a deal!',

];