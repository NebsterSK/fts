<?php

return [

    'title' => 'Cart',
    'empty' => 'Your cart is empty. You should change that...',
    'added' => 'Item added to cart.',
    'product' => 'Product',
    'price' => 'Price',
    'quantity' => 'Quantity',
    'subtotal' => 'Subtotal',
    'shipping' => 'Shipping',
    'total' => 'Total (with VAT)',
    'continue' => 'Continue shopping',
    'create' => 'Create an order',

];