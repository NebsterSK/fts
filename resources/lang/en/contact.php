<?php

return [

    'title' => 'Contact',
    'companyId' => 'Company ID',
    'oh' => 'Monday to Friday, 9:00 - 17:00 CEST',
    'name' => 'Name',
    'email' => 'E-mail',
    'message' => 'Message',
    'send' => 'Send',

];