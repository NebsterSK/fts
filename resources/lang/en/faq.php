<?php

return [

    'title' => 'Frequently Asked Questions',
    1 => 'What material are t-shirts made of?',
    '1a' => 'T-shirts are made of <strong>100% cotton</strong>.',
    2 => 'What temperature can t-shirts be washed at?',
    '2a' => 'Up to <strong>30 °C</strong>. You can wash them on higher temperature, however this will affect their lifespan.',
    3 => 'What are the payment options?',
    '3a' => 'We use <u>PayPal Smart Buttons</u> as our payment gateway. The payment options are determined automatically based on your country. But generally it should be possible to pay with <strong>Visa, Mastercard and PayPal account</strong>.',
    4 => 'What are the shipping options?',
    '4a' => 'So far we only ship orders via <strong>post office</strong>. More shipping options will be added in future.',
    5 => 'How long is the shipping time?',
    '5a' => 'Depends on the country of delivery. Usually should be <strong>up to a week</strong>.',
    6 => 'What about security?',
    '6a' => 'The connection with this website is secured with <strong>SSL certificate</strong> (<i>https://</i> in your browser\'s address bar).',
    '6a4' => 'payment gateway',
    '6a2' => 'ensures the highest possible standard in internet payments security. <u>We do NOT store any information you fill in PayPal payment gateway</u>.',
    '6a3' => 'If you would like to know more about what information we store and process, read our',

];