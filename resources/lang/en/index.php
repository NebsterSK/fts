<?php

return [

    'btn' => 'Check it out!',
    'quality' => 'Quality',
    1 => 'T-shirts are made of',
    2 => '100% cotton',
    3 => 'to ensure high quality, long durability and comfort.',
    4 => 'They are manufactured using "tunnel sewing", which means there are',
    5 => 'no joints on the sides',

];