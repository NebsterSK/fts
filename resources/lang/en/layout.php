<?php

return [

    'nav' => [
        'brand' => 'Brand',
        'products' => 'Products',
        'contact' => 'Contact',
        'login' => 'Log in',
        'register' => 'Register',
        'account' => 'My account',
        'logout' => 'Logout',
        'language' => 'Language',
        'cart' => 'Cart',
    ],

    'footer' => [
        'tac' => 'Terms & conditions',
        'pp' => 'Privacy policy',
        'car' => 'Claims & returns',
    ],

];