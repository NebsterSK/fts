<?php

return [

    'title' => 'Order',
    'title2' => 'Create',
    'products' => 'Products',
    'delivery' => 'Delivery',
    'read' => 'I have read and understand',
    'and' => 'and',
    'order' => 'Order & pay',

];