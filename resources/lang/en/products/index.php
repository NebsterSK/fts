<?php

return [

    'home' => 'Home',
    'title' => 'Products',
    'all' => 'All products',
    'colors' => 'Colors',
    'sizes' => 'Sizes',
    'white' => 'White',
    'black' => 'Black',
    'details' => 'Detail',
    'off' => ':percent% off',
    'inStock' => 'In stock',

];