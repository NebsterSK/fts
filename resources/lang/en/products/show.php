<?php

return [

    'gender' => 'Gender',
    'material' => 'Material',
    'cotton' => 'cotton',
    'weight' => 'Weight',
    'washable' => 'Washable on',
    'size' => 'Size',
    'price' => 'Price (with VAT)',
    'buy' => 'Buy',
    'inStockLong' => 'IN STOCK - ready for shipping',
    1 => 'Same day shipping',
    2 => 'Your order will be shipped the same day you pay for it, or next working day if you pay after our opening hours.',
    3 => 'Easy returns',
    4 => 'If you are not satisfied with our product just contact us and we will send you different size or money back.',
    5 => 'No registration needed',
    6 => 'Available sizes',
    7 => 'All size specifications listed are in centimeters. Accepted tolerance of up to +/- 7%.',

];