<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'title' => 'Belépés',
    'or' => 'vagy',
    'email' => 'E-mail',
    'password' => 'Jelszó',
    'remember' => 'Emlékezz rám',
    'forgot' => 'Elfelejtette a jelszavát?',

    'register' => 'Regisztráció',
    'name' => 'Név',
    'passwordConfirm' => 'Jelszó megerősítése',
    'read' => 'Elolvastam és megértettem',

    'fname' => 'Keresztnév',
    'lname' => 'Vezetéknév',
    'street' => 'Utca',
    'city' => 'Város',
    'zip' => 'Irányítószám',
    'country' => 'Ország',
    'phone' => 'Telefon',
    'note' => 'Jegyzet',

];
