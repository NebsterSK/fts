<?php

return [

    'title' => 'Márka',
    1 => 'a márkát 2019-ben alapította a Yasmin Trade s.r.o. kereskedelmi társaság, hogy eladja a nem formális ruházatot.',
    2 => 'A pólók 100% pamutból készülnek, biztosítva a magas minőséget, hosszú tartósságot és kényelmet. Az alagút varrásával készülnek, ami azt jelenti, hogy <u>nincsenek illesztések az oldalakon</u>.',
    3 => 'Főleg <strong>Közép-Európában</strong> kínálunk postakon keresztül történő szállítást. Ha messze vagy, de igazán érdekli termékeink, vegye fel velünk a kapcsolatot, és biztosak vagyunk abban, hogy megállapodást tudunk kötni!',

];