<?php

return array (

    'title' => 'Kocsi',
    'empty' => 'Az Ön kosara üres. Meg kell változtatnod ...',
    'added' => 'A termék bekerült a kosárba.',
    'product' => 'Termék',
    'price' => 'Ár',
    'quantity' => 'Mennyiség',
    'subtotal' => 'Részösszeg',
    'shipping' => 'Szállítás',
    'total' => 'Összesen (ÁFA-val)',
    'continue' => 'Folytatni a vásárlást',
    'create' => 'Hozzon létre egy megrendelést',

);