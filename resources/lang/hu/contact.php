<?php

return [

    'title' => 'Kapcsolatba lépni',
    'companyId' => 'Vállalati azonosító',
    'oh' => 'Hétfőtől péntekig, 9:00 - 17:00 CEST',
    'name' => 'Név',
    'email' => 'Email',
    'message' => 'Üzenet',
    'send' => 'Küld',

];