<?php

return [

    'title' => 'Gyakran Ismételt Kérdések',
    1 => 'Milyen anyagból készülnek pólók?',
    '1a' => 'A pólók <strong> 100% pamutból</strong> készülnek.',
    2 => 'Milyen hőmérsékleten lehet pólókat mosni?',
    '2a' => '<strong>30 ° C-ig</strong>. Magasabb hőmérsékleten moshatja, de ez befolyásolja élettartamukat.',
    3 => 'Milyen fizetési lehetőségek vannak?',
    '3a' => 'A <u>PayPal intelligens gombokat</u> használjuk fizetési átjárónkként. A fizetési lehetőségeket az ország határozza meg automatikusan. De általában a <strong>Visa, Mastercard és PayPal számlákkal</strong> kell fizetni.',
    4 => 'Milyen szállítási lehetőségek vannak?',
    '4a' => 'Eddig csak a <strong>posta</strong> -on küldünk megrendeléseket. További szállítási lehetőségeket fogunk hozzáadni a jövőben.',
    5 => 'Mennyi a szállítási idő?',
    '5a' => 'A szállítás országától függ. Általában <strong>legfeljebb egy hétig</strong> kell lennie.',
    6 => 'Mi a helyzet a biztonsággal?',
    '6a' => 'A kapcsolatot ezzel a webhellyel <strong>SSL tanúsítvány</strong> biztosítja (<i>https://</i> a böngésző címsorában).',
    '6a4' => 'fizetési átjáró',
    '6a2' => 'biztosítja a lehető legmagasabb szintű internetes fizetési biztonságot. <u> NEM tárolunk semmilyen információt, amelyet kitöltött a PayPal fizetési átjárón </u>.',
    '6a3' => 'Ha többet szeretne tudni arról, hogy milyen információkat tárolunk és dolgozunk fel, olvassa el a',

];