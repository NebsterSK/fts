<?php

return [

    'btn' => 'Nézd meg!',
    'quality' => 'Minőség',
    1 => 'A pólók',
    2 => '100% pamutból',
    3 => 'készülnek, biztosítva a magas minőséget, hosszú tartósságot és kényelmet.',
    4 => 'Ezeket "alagút varrással" gyártják, ami azt jelenti, hogy az oldalakban',
    5 => 'nincsenek illesztések',

];