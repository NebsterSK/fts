<?php

return [

    'nav' => [
        'brand' => 'Márka',
        'products' => 'Termékek',
        'contact' => 'Érintkezés',
        'login' => 'Belépés',
        'register' => 'Regisztráció',
        'account' => 'Az én fiókom',
        'logout' => 'Kijelentkezés',
        'language' => 'Nyelv',
        'cart' => 'Kocsi',
    ],

    'footer' => [
        'tac' => 'Felhasználási feltételek',
        'pp' => 'Adatvédelmi irányelvek',
        'car' => 'Követelések és visszatérítések',
    ],

];