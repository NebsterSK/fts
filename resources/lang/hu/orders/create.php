<?php

return [

    'title' => 'Rendelés',
    'title2' => 'Teremt',
    'products' => 'Termékek',
    'delivery' => 'Kézbesítés',
    'read' => 'Elolvastam és megértettem',
    'and' => 'és',
    'order' => 'Megrendelés és fizetés',

];