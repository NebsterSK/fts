<?php

return [

    'title2' => 'Fizetés',
    'methods' => 'Fizetési módok',
    'redirecting' => 'Átirányítás ...',
    'store' => 'Az alábbiakban NEM tároljuk a PayPal fizetési átjárójában kitöltött adatokat.',

];