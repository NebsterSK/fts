<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A jelszavaknak legalább nyolc karakterből kell állniuk, és egyezniük kell a megerősítéssel.',
    'reset' => 'A jelszavad vissza lett állítva!',
    'sent' => 'E-mailben elküldtük a jelszó-visszaállítási linket!',
    'token' => 'Ez a jelszó-visszaállítási token érvénytelen.',
    'user' => "Nem találunk egy felhasználót e-mail címmel.",

];
