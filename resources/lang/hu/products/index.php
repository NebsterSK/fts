<?php

return [

    'home' => 'Főoldal',
    'title' => 'Termékek',
    'all' => 'Minden termék',
    'colors' => 'Színek',
    'sizes' => 'Méretek',
    'white' => 'Fehér',
    'black' => 'Fekete',
    'details' => 'Részlet',
    'off' => ':percent% kedvezmény',
    'inStock' => 'Raktráron',

];