<?php

return [

    'gender' => 'Nem',
    'material' => 'Anyag',
    'cotton' => 'pamut',
    'weight' => 'Súly',
    'washable' => 'Mosható',
    'size' => 'Méret',
    'price' => 'Ár (áfa)',
    'buy' => 'Megvesz',
    'inStockLong' => 'RAKTÁRON - készen áll a szállításra',
    1 => 'Ugyanazon a napon szállítás',
    2 => 'Megrendelését azon a napon küldjük el, amikor fizeti azért, vagy a következő munkanapon, ha fizet a nyitvatartási idő után.',
    3 => 'Könnyű visszatérés',
    4 => 'Ha nem elégedett termékkel, vegye fel velünk a kapcsolatot, és különféle méretű vagy pénz-visszatérítést küldünk Önnek.',
    5 => 'Nincs szükség regisztrációra',
    6 => 'Elérhető méretek',
    7 => 'Az összes felsorolt ​​méretleírás centiméterben található. Engedélyezett tűrés +/- 7%.',

];