<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Te dane uwierzytelniające nie pasują do naszych danych.',
    'throttle' => 'Zbyt wiele prób logowania. Spróbuj ponownie za :seconds sekundy.',

    'logIn' => 'Zaloguj sie',
    'or' => 'LUB',
    'email' => 'E-mail',
    'password' => 'Hasło',
    'remember' => 'Zapamiętaj mnie',
    'forgot' => 'Zapomniałeś hasła?',

    'register' => 'Zarejestrować',
    'name' => 'Imię',
    'passwordConfirm' => 'Potwierdź hasło',
    'read' => 'Przeczytałem i rozumiem',

    'fname' => 'Imię',
    'lname' => 'Nazwisko',
    'street' => 'Ulica',
    'city' => 'Miasto',
    'zip' => 'Kod pocztowy',
    'country' => 'Kraj',
    'phone' => 'Telefon',
    'note' => 'Uwaga',

];
