<?php

return [

    'title' => 'Marka',
    1 => 'Marka została założona w 2019 roku przez firmę handlową Yasmin Trade s.r.o. w celu sprzedaży odzieży nieformalnej.',
    2 => 'Koszulki wykonane są w 100% z bawełny, aby zapewnić wysoką jakość, długą trwałość i komfort. Są wytwarzane przy użyciu „szycia tunelowego”, co oznacza <u>po bokach nie ma połączeń</u>.',
    3 => 'Oferujemy wysyłkę za pośrednictwem poczty głównie w <strong>Europie Środkowej</strong>. Jeśli jesteś z daleka, ale naprawdę interesują Cię nasze produkty, skontaktuj się z nami, a my na pewno możemy zawrzeć umowę!',

];