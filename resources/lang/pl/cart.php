<?php

return [

    'title' => 'Wózek',
    'empty' => 'Twój koszyk jest pusty. Powinieneś to zmienić ...',
    'added' => 'Produkt dodany do koszyka.',
    'product' => 'Produkt',
    'price' => 'Cena £',
    'quantity' => 'Ilość',
    'subtotal' => 'Suma częściowa',
    'shipping' => 'Wysyłka',
    'total' => 'Razem (z VAT)',
    'continue' => 'Kontynuować zakupy',
    'create' => 'Utwórz zamówienie',

];