<?php

return [

    'title' => 'Kontakt',
    'companyId' => 'Identyfikator firmy',
    'oh' => 'Od poniedziałku do piątku w godzinach 9:00 - 17:00 CEST',
    'name' => 'Imię',
    'email' => 'E-mail',
    'message' => 'Wiadomość',
    'send' => 'Wysłać',

];