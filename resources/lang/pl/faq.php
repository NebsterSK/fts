<?php

return array (

    'title' => 'Często Zadawane Pytania',
    1 => 'Z jakiego materiału wykonane są koszulki?',
    '1a' => 'Koszulki wykonane są z <strong>100% bawełny</strong>.',
    2 => 'W jakiej temperaturze można prać koszulki?',
    '2a' => 'Do <strong>30 ° C</strong>. Można je prać w wyższej temperaturze, jednak wpłynie to na ich żywotność.',
    3 => 'Jakie są opcje płatności?',
    '3a' => 'Używamy <u>inteligentnych przycisków PayPal</u> jako naszej bramki płatności. Opcje płatności są określane automatycznie na podstawie twojego kraju. Ale ogólnie powinno być możliwe płacenie za pomocą konta <strong> Visa, Mastercard i PayPal </strong>.',
    4 => 'Jakie są opcje wysyłki?',
    '4a' => 'Do tej pory wysyłamy zamówienia tylko przez <strong>pocztę</strong>. Więcej opcji wysyłki zostaną dodane w przyszłości.',
    5 => 'Jak długi jest czas wysyłki?',
    '5a' => 'Zależy od kraju dostawy. Zwykle powinno to być <strong>do tygodnia</strong>.',
    6 => 'Co z bezpieczeństwem?',
    '6a' => 'Połączenie z tą witryną jest zabezpieczone <strong>certyfikatem SSL</strong> (<i>https://</i> w pasku adresu przeglądarki).',
    '6a4' => 'Bramki płatności',
    '6a2' => 'zapewnia najwyższy możliwy standard bezpieczeństwa płatności internetowych. <u>NIE przechowujemy żadnych informacji, które wpisujesz w bramce płatności PayPal</u>.',
    '6a3' => 'Jeśli chcesz dowiedzieć się więcej o tym, jakie informacje przechowujemy i przetwarzamy, przeczytaj nasze',

);