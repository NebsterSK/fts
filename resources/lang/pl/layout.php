<?php

return [

    'nav' =>[
        'brand' => 'Marka',
        'products' => 'Produkty',
        'contact' => 'Kontakt',
        'login' => 'Zaloguj sie',
        'register' => 'Zarejestrować',
        'account' => 'Moje konto',
        'logout' => 'Wyloguj',
        'language' => 'Język',
        'cart' => 'Wózek',
    ],

    'footer' => [
        'tac' => 'Zasady i Warunki',
        'pp' => 'Polityka prywatności',
        'car' => 'Roszczenia i zwroty',
    ],

];