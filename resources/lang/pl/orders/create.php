<?php

return [

    'title' => 'Zamówienie',
    'title2' => 'Stwórz',
    'products' => 'Produkty',
    'delivery' => 'Dostawa',
    'read' => 'Przeczytałem i rozumiem',
    'and' => 'i',
    'order' => 'Zamów i zapłać',

];