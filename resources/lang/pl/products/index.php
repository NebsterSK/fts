<?php

return [

    'home' => 'Strona główna',
    'title' => 'Produkty',
    'all' => 'Wszystkie produkty',
    'colors' => 'Zabarwienie',
    'sizes' => 'Rozmiary',
    'white' => 'Biały',
    'black' => 'Czarny',
    'details' => 'Szczegół',
    'off' => ':percent% zniżki',
    'inStock' => 'W magazynie',

];