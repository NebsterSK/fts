<?php

return [

    'gender' => 'Płeć',
    'material' => 'Materiał',
    'cotton' => 'bawełna',
    'weight' => 'Waga',
    'washable' => 'Można prać',
    'size' => 'Rozmiar',
    'price' => 'Cena (z VAT)',
    'buy' => 'Kupować',
    'inStockLong' => 'W MAGAZYNIE - gotowe do wysyłki',
    1 => 'Wysyłka tego samego dnia',
    2 => 'Twoje zamówienie zostanie wysłane tego samego dnia, w którym je zapłacisz, lub następnego dnia roboczego, jeśli zapłacisz po naszych godzinach otwarcia.',
    3 => 'Łatwe zwroty',
    4 => 'Jeśli nie jesteś zadowolony z naszego produktu, skontaktuj się z nami, a my wyślemy ci inny rozmiar lub zwrot pieniędzy.',
    5 => 'Nie wymaga rejestracji',
    6 => 'Dostępne rozmiary',
    7 => 'Wszystkie specyfikacje dotyczące wielkości podano w centymetrach. Akceptowana tolerancja do +/- 7%.',

];