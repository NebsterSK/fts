<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Zadané údaje nie su správne',
    'throttle' => 'Príliš mnoho pokusov ao prihlásenie. Skúste to o :seconds sekúnd.',

    'logIn' => 'Prihlásiť',
    'or' => 'alebo',
    'email' => 'E-mail',
    'password' => 'Heslo',
    'remember' => 'Zapamätať prihlásenie',
    'forgot' => 'Zabudnuté heslo?',

    'register' => 'Registrácia',
    'name' => 'Meno',
    'passwordConfirm' => 'Potvrdiť heslo',
    'read' => 'Prečítal som si a rozumiem',

    'fname' => 'Meno',
    'lname' => 'Priezvisko',
    'street' => 'Ulica',
    'city' => 'Mesto',
    'zip' => 'PSČ',
    'country' => 'Krajina',
    'phone' => 'Telefón',
    'note' => 'Poznámka',

];
