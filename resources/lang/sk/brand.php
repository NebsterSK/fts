<?php

return [

    'title' => 'Značka',
    1 => 'značka bola založená v roku 2019 obchodnou spoločnosťou Yasmin Trade s.r.o. na predaj neformálneho oblečenia.',
    2 => 'Tričká sú vyrobené zo 100% bavlny, aby bola zaistená vysoká kvalita, dlhá životnosť a pohodlie. Šú vyrávbané pomocou „tunelového šitia“, čo znamená, že na bokoch nie sú žiadne spoje.',
    3 => 'Ponúkame doručenie poštou hlavne v <strong>strednej Európe</strong>. Ak ste zďaleka ale skutočne sa zaujímate o naše výrobky, kontaktujte nás a určite sa dohodneme!',

];