<?php

return [

    'title' => 'Košík',
    'empty' => 'Váš košík je prázdny. Mali by ste to zmeniť...',
    'added' => 'Položka bola pridaná do košíka.',
    'product' => 'Produkt',
    'price' => 'Cena',
    'quantity' => 'Množstvo',
    'subtotal' => 'Medzisúčet',
    'shipping' => 'Doprava',
    'total' => 'Celkom (s DPH)',
    'continue' => 'Pokračovať v nákupe',
    'create' => 'Vytvoriť objednávku',

];