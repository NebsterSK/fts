<?php

return [

    'title' => 'Kontakt',
    'companyId' => 'IČO',
    'oh' => 'Pondelok až piatok, 9:00 - 17:00 CEST',
    'name' => 'Meno',
    'email' => 'E-mail',
    'message' => 'Správa',
    'send' => 'Odoslať',

];