<?php

return [

    'title' => 'Často kladené otázky',
    1 => 'Z akého materiálu sú tričká vyrobené?',
    '1a' => 'Tričká sú vyrobené zo <strong>100% bavlny</strong>.',
    2 => 'Pri akej teplote sa dajú tričká prať?',
    '2a' => 'Až do <strong>30 °C</strong>. Môžete ich prať pri vyššej teplote, to však ovplyvní ich životnosť.',
    3 => 'Aké sú možnosti platby?',
    '3a' => 'Ako platobnú bránu používame <u>inteligentné tlačidlá PayPal</u>. Možnosti platby sú stanovené automaticky podľa vašej krajiny. Vo všeobecnosti by však malo byť možné platiť <strong>Visa / Mastercard kartami a PayPal účtu</strong>.',
    4 => 'Aké sú možnosti dopravy?',
    '4a' => 'Zatiaľ posielame objednávky iba prostredníctvom <strong>pošty</strong>. V budúcnosti pribudnú ďalšie možnosti dopravy.',
    5 => 'Aká je doba doručenia?',
    '5a' => 'To závisí od krajiny dodania. Zvyčajne by mal byť <strong>do týždňa</strong>.',
    6 => 'A čo bezpečnosť?',
    '6a' => 'Spojenie s touto webovou stránkou je zabezpečené pomocou <strong>SSL certifikátu</strong> (<i>https://</i> v paneli s adresou v prehliadači).',
    '6a4' => 'platobná brána',
    '6a2' => 'zaisťuje najvyššiu možnú úroveň zabezpečenia internetových platieb. <u>NEUKLADÁME žiadne informácie, ktoré vyplníte v platobnej bráne PayPal</u>.',
    '6a3' => 'Ak sa chcete dozvedieť viac o tom, aké informácie ukladáme a spracúvame, prečítajte si naše',

];