<?php

return [

    'btn' => 'Pozri na to!',
    'quality' => 'Kvalita',
    1 => 'Tričká sú vyrobené z',
    2 => '100% bavlny',
    3 => 'čo zaručuje vysokú kvalitu, dlhú životnosť a pohodlie.',
    4 => 'Sú vyrábané pomocou „tunelového šitia“, čo znamená že po bokoch',
    5 => 'nie su žiadne spoje',

];