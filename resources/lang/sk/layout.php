<?php

return [

    'nav' => [
        'brand' => 'Značka',
        'products' => 'Produkty',
        'contact' => 'Kontakt',
        'login' => 'Prihlásenie',
        'register' => 'Registrácia',
        'account' => 'Môj účet',
        'logout' => 'Odhlásiť sa',
        'language' => 'Jazyk',
        'cart' => 'Košík',
      ],

    'footer' => [
        'tac' => 'Obchodné podmienky',
        'pp' => 'Zásady ochrany osobných údajov',
        'car' => 'Reklamácie',
      ],

];