<?php

return [

    'title' => 'Objednávka',
    'title2' => 'Vytvoriť',
    'products' => 'Produkty',
    'delivery' => 'Dodanie',
    'read' => 'Čítal som a rozumiem',
    'and' => 'a',
    'order' => 'Objednať a zaplatiť',

];