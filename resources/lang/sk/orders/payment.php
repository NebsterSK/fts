<?php

return [

    'title2' => 'Platba',
    'methods' => 'Spôsob platby',
    'redirecting' => 'Presmerovanie ...',
    'store' => 'Neuchovávame žiadne údaje vyplnené do platobnej brány PayPal.',

];