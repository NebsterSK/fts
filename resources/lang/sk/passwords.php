<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Heslo musí mať aspoň 8 znakov a zhodovať sa s potvrdením.',
    'reset' => 'Vaše heslo bolo zmenené!',
    'sent' => 'Poslali sme Vám link na zmenu hesla!',
    'token' => 'Neplatný token na zmenu hesla.',
    'user' => "Nemôžeme nájsť užívateľa s takýmto e-mailom.",

];
