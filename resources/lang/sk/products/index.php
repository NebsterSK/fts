<?php

return [

    'home' => 'Úvod',
    'title' => 'Produkty',
    'all' => 'Všetky produkty',
    'colors' => 'Farby',
    'sizes' => 'Veľkosti',
    'white' => 'Biela',
    'black' => 'Čierna',
    'details' => 'Detail',
    'off' => ':percent% zľava',
    'inStock' => 'Skladom',

];