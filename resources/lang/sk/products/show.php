<?php

return [

    'gender' => 'Pohlavie',
    'material' => 'Materiál',
    'cotton' => 'bavlna',
    'weight' => 'Váha',
    'washable' => 'Prať do',
    'size' => 'Veľkosť',
    'price' => 'Cena (s DPH)',
    'buy' => 'Kúpiť',
    'inStockLong' => 'SKLADOM - pripravené na odoslanie',
    1 => 'Odoslanie v ten istý deň',
    2 => 'Vaša objednávka bude odoslaná v ten istý deň, v ktorý zaplatíte, alebo nasledujúci pracovný deň, ak platíte po našich otváracích hodinách.',
    3 => 'Jednoduchá reklamácia',
    4 => 'Ak nie ste spokojní s naším produktom, kontaktujte nás a my vám pošleme inú veľkosť alebo peniaze späť.',
    5 => 'Nie je potrebná registrácia',
    6 => 'Dostupné veľkosti',
    7 => 'Všetky uvedené veľkosti sú uvedené v centimetroch. Tolerancia +/- 7%.',

];