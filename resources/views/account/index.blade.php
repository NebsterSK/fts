@extends('layouts/public')

@section('title', 'Account settings - ' . config('app.name'))

@section('startBody')
    <div id="deleteModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete account</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form action="{{ route('account.delete') }}" id="deleteForm" method="POST">
                        @csrf

                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="check" name="check" required>

                            <label class="form-check-label" for="check">
                                I wish to delete my account and all information associated with it.
                            </label>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                    <button type="submit" form="deleteForm" class="btn btn-danger">Delete account</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div id="page-content">
        <div class="container pt-2 pt-lg-5 pb-5">
            <h1>My account</h1>

            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" href="">Account information</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('account.orders.index') }}">Orders</a>
                </li>
            </ul>

            <div class="row mt-2">
                <div class="col-12 col-lg-6">
                    @include('includes/errors')

                    <form action="{{ route('account.update') }}" method="POST">
                        @csrf

                        @method('PUT')

                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label for="name">Name</label>

                                    <input id="name" type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" required maxlength="20">
                                </div>
                            </div>

                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label for="surname">Surname</label>

                                    <input id="surname" type="text" class="form-control" name="surname" value="{{ Auth::user()->surname }}" required maxlength="30">
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label for="password">Change password</label>

                                    <input id="password" type="password" class="form-control" name="password" minlength="8" maxlength="20">
                                </div>
                            </div>

                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label for="password-confirm">Confirm password</label>

                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" minlength="8" maxlength="20">
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-first">Save</button>
                    </form>
                </div>

                <div class="col-12 col-lg-6">
                    <h2 class="color-first">GDPR</h2>

                    <div class="row">
                        <div class="col-6">
                            <p>Download all your account information in JSON format.</p>

                            <a href="{{ route('account.download') }}" class="btn btn-info btn-block">@fa('download') Download account information</a>
                        </div>

                        <div class="col-6">
                            <p>Permanently delete your account from our database.</p>

                            <a href="" class="btn btn-danger btn-block" data-toggle="modal" data-target="#deleteModal">@fa('times') Delete account</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection