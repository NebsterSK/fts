@extends('layouts/public')

@section('title', 'Orders - ' . config('app.name'))

@section('content')
    <div id="page-content">
        <div class="container pt-2 pt-lg-5 pb-5">
            <h1>My account</h1>

            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('account.index') }}">Account information</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link active" href="">Orders</a>
                </li>
            </ul>

            <table class="table table-striped table-bordered mt-2">
                <thead>
                <tr class="text-center">
                    <th>Date & time</th>

                    <th>ID</th>

                    <th>Price</th>

                    <th>Status</th>

                    <th></th>
                </tr>
                </thead>

                <tbody>
                @each('components/account/order', $arrOrders, 'objOrder')
                </tbody>
            </table>
        </div>
    </div>
@endsection