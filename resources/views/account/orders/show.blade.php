@extends('layouts/public')

@section('title', 'Order - ' . config('app.name'))

@section('content')
    <div id="page-content">
        <div class="container-fluid pt-2 pb-5">
            <a href="{{ route('account.orders.index') }}">@fa('arrow-left') Back to Orders</a>

            <h2>Order #{{ $objOrder->id }}</h2>

            <div class="row">
                <div class="col-12 col-lg-3">
                    <div class="card">
                        <header class="card-header">
                            <h4 class="card-title mb-0">Details</h4>
                        </header>

                        <div class="card-body">
                            <p><strong>Created at:</strong> {{ $objOrder->created_at->format('j.n.Y H:i:s') }}</p>

                            <p><strong>Status:</strong> {{ $objOrder->status }}</p>

                            <hr>

                            <p><strong>Name:</strong> {{ $objOrder->full_name }}</p>

                            <p><strong>Address:</strong> {{ $objOrder->address }}</p>

                            <p><strong>E-mail:</strong> <a href="mailto:{{ $objOrder->email }}">{{ $objOrder->email }}</a></p>

                            <p><strong>Phone:</strong> {{ $objOrder->phone }}</p>

                            <p class="mb-0"><strong>Note:</strong> {{ $objOrder->note }}</p>
                        </div>

                        <footer class="card-footer">
                            @if($objOrder->paid)
                                <a href="{{ route('account.orders.invoice', ['order' => $objOrder->id]) }}" class="btn btn-first btn-block">@fa('download') Download invoice</a>
                            @else
                                <div class="alert alert-info">
                                    @fa('info-circle') This order has not been paid yet.
                                </div>

                                <a href="{{ route('orders.payment', ['order' => $objOrder->id]) }}" class="btn btn-first btn-block">Go to payment</a>
                            @endif
                        </footer>
                    </div>
                </div>

                <div class="col-12 col-lg-9">
                    <h4>Items</h4>

                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr class="text-center">
                            <th>Product</th>

                            <th>Color</th>

                            <th>Size</th>

                            <th>Amount</th>

                            <th>Price per unit</th>

                            <th>Price</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($objOrder->ordered_products as $objItem)
                            <tr>
                                <td>{{ $objItem->product->name }}</td>

                                <td class="text-capitalize">{{ $objItem->colorName }}</td>

                                <td class="text-uppercase">{{ $objItem->size->name }}</td>

                                <td>{{ $objItem->amount }}</td>

                                <td class="text-right">&euro; {{ number_format($objItem->product->price, 2, ',', ' ') }}</td>

                                <td class="text-right">&euro; {{ number_format($objItem->amount * $objItem->product->price, 2, ',', ' ') }}</td>
                            </tr>
                        @endforeach

                        <tr class="text-right">
                            <td colspan="5">Shipping</td>

                            <td>&euro; 1,00</td>
                        </tr>

                        <tr class="h4 font-weight-bold text-right">
                            <td colspan="5">Total</td>

                            <td>&euro; {{ number_format($objOrder->total, 2, ',', ' ') }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection