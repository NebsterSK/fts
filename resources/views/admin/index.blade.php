@extends('layouts/admin')

@section('addCSS')
    <meta http-equiv="refresh" content="60">
@endsection

@section('content')
    <div id="page-content" class="admin">
        <div class="container-fluid pt-2 pb-5">
            <div class="row">
                <div class="col-6">
                    <h2>Newest orders <span class="badge badge-dark">{{ $arrOrders->count() }}</span></h2>

                    <table class="table table-striped table-bordered table-sm">
                        <thead>
                        <tr class="text-center">
                            <th>Created at</th>

                            <th>Status</th>

                            <th>Name</th>

                            <th>Price</th>

                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                            @each('components/admin/orders/newest', $arrOrders, 'objOrder')
                        </tbody>
                    </table>
                </div>

                <div class="col-3">
                    <h2>Newest users <span class="badge badge-dark">{{ $arrUsers->count() }}</span></h2>

                    <table class="table table-striped table-bordered table-sm">
                        <thead>
                        <tr class="text-center">
                            <th>Created at</th>

                            <th>Name</th>

                            <th>E-mail</th>
                        </tr>
                        </thead>

                        <tbody>
                            @each('components/admin/users/newest', $arrUsers, 'objUser')
                        </tbody>
                    </table>
                </div>

                <div class="col-3">
                    <h2>Top selling products <span class="badge badge-dark">{{ $arrProducts->count() }}</span></h2>

                    <div class="mb-2">
                        {!! $objChart->container() !!}
                    </div>

                    <table class="table table-bordered table-striped table-sm">
                        <thead>
                        <tr class="text-center">
                            <th>Name</th>

                            <th>Color</th>

                            <th>Size</th>

                            <th>Amount</th>

                            <th>Percentage</th>
                        </tr>
                        </thead>

                        <tbody>
                            @each('components/admin/topSellingProductsChartItem', $arrProducts, 'objProduct')
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('addJS')
    {!! $objChart->script() !!}
@endsection