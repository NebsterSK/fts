@extends('layouts/admin')

@section('content')
    <div id="page-content" class="admin">
        <div class="container-fluid pt-2 pb-5">
            <h2>Orders <span class="badge badge-dark">{{ $arrOrders->count() }} / {{ $arrOrders->total() }}</span></h2>

            <table class="table table-striped table-bordered">
                <thead>
                <tr class="text-center">
                    <th>Date & time</th>

                    <th>Status</th>

                    <th>Name</th>

                    <th>Address</th>

                    <th>E-mail</th>

                    <th>Phone</th>

                    <th>Price</th>

                    <th></th>
                </tr>
                </thead>

                <tbody>
                    @each('components/admin/orders/order', $arrOrders, 'objOrder')
                </tbody>
            </table>
        </div>
    </div>
@endsection