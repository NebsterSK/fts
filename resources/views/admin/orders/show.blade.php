@extends('layouts/admin')

@section('content')
    <div id="page-content" class="admin">
        <div class="container-fluid pt-2 pb-5">
            <h2>Order #{{ $objOrder->id }}</h2>

            <div class="row">
                <div class="col-12 col-lg-2">
                    <div class="card">
                        <header class="card-header">
                            <h4 class="card-title mb-0">Details</h4>
                        </header>

                        <div class="card-body">
                            <p><strong>Created:</strong> {{ $objOrder->created_at->format('j.n.Y H:i:s') }}</p>

                            <form action="{{ route('admin.orders.status', ['id' => $objOrder->id]) }}" method="POST">
                                @csrf

                                @method('PUT')

                                <div class="input-group">
                                    <select class="form-control" id="status" name="status">
                                        @component('components/admin/orders/statusSelector', ['status' => $objOrder->status])@endcomponent
                                    </select>

                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Save</button>
                                    </div>
                                </div>
                            </form>

                            <hr>

                            <p>
                                <strong>Name:</strong>
                                @if($objOrder->user_id)
                                    <a href="{{ route('admin.users.show', ['id' => $objOrder->user_id]) }}">{{ $objOrder->full_name }}</a>
                                @else
                                    {{ $objOrder->full_name }}
                                @endif
                            </p>

                            <p>
                                <strong>Address:</strong><br>{{ $objOrder->street }}<br>{{ $objOrder->zip }}, {{ $objOrder->city }}<br>{{ $objOrder->country }}
                            </p>

                            <p><strong>E-mail:</strong> <a href="mailto:{{ $objOrder->email }}">{{ $objOrder->email }}</a></p>

                            <p><strong>Phone:</strong> {{ $objOrder->phone }}</p>

                            <p class="mb-0"><strong>Note:</strong> {{ $objOrder->note }}</p>
                        </div>

                        @if($objOrder->paid)
                            <footer class="card-footer">
                                <a href="{{ route('admin.orders.invoice', ['order' => $objOrder->id]) }}" class="btn btn-first btn-block">@fa('download') Download invoice</a>
                            </footer>
                        @endif
                    </div>
                </div>

                <div class="col-12 col-lg-2">
                    <h4>Comments</h4>

                    <form action="{{ route('admin.comments.store') }}" method="POST">
                        @csrf

                        <input type="hidden" name="commentable_id" value="{{ $objOrder->id }}">

                        <input type="hidden" name="commentable_type" value="App\Order">

                        <div class="form-group">
                            <label for="text">Message</label>
                            <textarea class="form-control" id="text" name="text" maxlength="300" rows="2" required></textarea>
                        </div>

                        <button type="submit" class="btn btn-first btn-block">Save</button>
                    </form>

                    @each('components/admin/comments/comment', $objOrder->comments->reverse(), 'objComment')
                </div>

                <div class="col-12 col-lg-8">
                    <h4>Items</h4>

                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr class="text-center">
                            <th>Product</th>

                            <th>Color</th>

                            <th>Size</th>

                            <th>Amount</th>

                            <th>Price per unit</th>

                            <th>Price</th>
                        </tr>
                        </thead>

                        <tbody>
                            @each('components/admin/orders/productItem', $objOrder->ordered_products, 'objItem')

                            <tr class="text-right">
                                <td colspan="5">Shipping</td>

                                <td>&euro; 2,00</td>
                            </tr>

                            <tr class="h4 font-weight-bold text-right">
                                <td colspan="5">Total</td>

                                <td>&euro; {{ number_format($objOrder->total, 2, ',', ' ') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection