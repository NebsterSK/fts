@extends('layouts/admin')

@section('content')
    <div id="page-content" class="admin">
        <div class="container pt-2 pb-5">
            <h2>User #{{ $objUser->id }} edit</h2>

            <form action="{{ route('admin.users.update', ['id' => $objUser->id]) }}" method="POST">
                @csrf

                @method('PUT')

                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <label for="name">Name</label>

                            <input id="name" type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" required maxlength="20">
                        </div>
                    </div>

                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <label for="surname">Surname</label>

                            <input id="surname" type="text" class="form-control" name="surname" value="{{ Auth::user()->surname }}" required maxlength="30">
                        </div>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <label for="password">New password</label>

                            <input id="password" type="password" class="form-control" name="password" minlength="8" maxlength="20">
                        </div>
                    </div>

                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <label for="password-confirm">Confirm password</label>

                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" minlength="8" maxlength="20">
                        </div>
                    </div>
                </div>

                <div class="text-right">
                    <button type="submit" class="btn btn-first">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection