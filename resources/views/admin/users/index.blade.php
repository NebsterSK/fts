@extends('layouts/admin')

@section('content')
    <div id="page-content" class="admin">
        <div class="container-fluid pt-2 pb-5">
            <h2>Users <span class="badge badge-dark">{{ $arrUsers->count() }} / {{ $arrUsers->total() }}</span></h2>

            <table class="table table-striped table-bordered">
                <thead>
                <tr class="text-center">
                    <th>ID</th>

                    <th>Name</th>

                    <th>E-mail</th>
                </tr>
                </thead>

                <tbody>
                    @each('components/admin/users/user', $arrUsers, 'objUser')
                </tbody>
            </table>
        </div>
    </div>
@endsection