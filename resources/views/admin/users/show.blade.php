@extends('layouts/admin')

@section('content')
    <div id="page-content" class="admin">
        <div class="container-fluid pt-2 pb-5">
            <h2>User #{{ $objUser->id }}</h2>

            <div class="row">
                <div class="col-12 col-lg-2">
                    <div class="card">
                        <header class="card-header">
                            <a href="{{ route('admin.users.edit', ['id' => $objUser->id]) }}" class="btn btn-first btn-sm float-right">Edit</a>

                            <h4 class="card-title mb-0">Details</h4>
                        </header>

                        <div class="card-body">
                            <p><strong>Created:</strong> {{ $objUser->created_at->format('j.n.Y H:i') }}</p>

                            <hr>

                            <p><strong>Name:</strong> {{ $objUser->full_name }}</p>

                            <p class="mb-0"><strong>E-mail:</strong> <a href="mailto:{{ $objUser->email }}">{{ $objUser->email }}</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-2">
                    <h4>Comments</h4>

                    <form action="{{ route('admin.comments.store') }}" method="POST">
                        @csrf

                        <input type="hidden" name="commentable_id" value="{{ $objUser->id }}">

                        <input type="hidden" name="commentable_type" value="App\User">

                        <div class="form-group">
                            <label for="text">Message</label>
                            <textarea class="form-control" id="text" name="text" maxlength="300" rows="2" required></textarea>
                        </div>

                        <button type="submit" class="btn btn-first btn-block">Save</button>
                    </form>

                    @each('components/admin/comments/comment', $objUser->comments->reverse(), 'objComment')
                </div>

                <div class="col-12 col-lg-8">
                    <h4>Orders</h4>

                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr class="text-center">
                            <th>ID</th>

                            <th>Created at</th>

                            <th>Status</th>

                            <th>Address</th>

                            <th>Price</th>

                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                            @each('components/admin/users/orderItem', $objUser->orders->reverse(), 'objOrder')
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection