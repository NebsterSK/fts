@extends('layouts/public')

@section('title', 'Log in - ' . config('app.name'))

@section('content')
    <div id="page-content">
        <div class="container pt-2 pt-lg-5 pb-5">
            <div class="row">
                <div class="col-6 offset-3 col-md-12 offset-md-0">
                    <img src="{{ mix('images/logos/250x250.png') }}" class="img-fluid d-block mx-auto mb-2 mb-lg-5" alt="">
                </div>
            </div>

            <h1 class="text-center mb-3">@lang('auth.logIn')</h1>

            <div class="row">
                <div class="col-12 col-md-6 offset-md-3 col-lg-4 offset-lg-4">
                    <a href="{{ route('fb') }}" class="btn btn-fb btn-block btn-lg">@fa('facebook') Facebook</a>

                    <p class="text-center lead mt-3 mb-3">@lang('auth.or')</p>

                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group">
                            <label for="email">@lang('auth.email')</label>

                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="password">@lang('auth.password')</label>


                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="text-center">
                            <div class="form-check mb-2">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">@lang('auth.remember')</label>
                            </div>

                            <button type="submit" class="btn btn-first btn-block mb-2">@lang('auth.logIn')</button>

                            <a class="btn btn-link" href="{{ route('password.request') }}">@lang('auth.forgot')</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
