@extends('layouts/public')

@section('title', 'Reset password - E-mail - ' . config('app.name'))

@section('content')
    <div id="page-content">
        <div class="container pt-2 pt-lg-5 pb-5">
            <div class="row">
                <div class="col-6 offset-3 col-md-12 offset-md-0">
                    <img src="{{ mix('images/logos/250x250.png') }}" class="img-fluid d-block mx-auto mb-2 mb-lg-5" alt="">
                </div>
            </div>

            <h1 class="text-center mb-3">Reset password</h1>

            <div class="row">
                <div class="col-12 col-md-6 offset-md-3 col-lg-4 offset-lg-4">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group">
                            <label for="email">E-mail</label>

                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-first btn-block">Send me password reset link</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
