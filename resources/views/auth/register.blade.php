@extends('layouts/public')

@section('title', 'Register - ' . config('app.name'))

@section('content')
    <div id="page-content">
        <div class="container pt-2 pt-lg-5 pb-5">
            <div class="row">
                <div class="col-6 offset-3 col-md-12 offset-md-0">
                    <img src="{{ mix('images/logos/250x250.png') }}" class="img-fluid d-block mx-auto mb-2 mb-lg-5" alt="">
                </div>
            </div>

            <h1 class="text-center mb-3">@lang('auth.register')</h1>

            <div class="row">
                <div class="col-12 col-md-6 offset-md-3 col-lg-4 offset-lg-4">
                    <a href="{{ route('fb') }}" class="btn btn-fb btn-block btn-lg">@fa('facebook') Facebook</a>

                    <p class="text-center lead mt-3 mb-3">@lang('auth.or')</p>

                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group">
                            <label for="name">@lang('auth.name')</label>

                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="email">@lang('auth.email')</label>

                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="password">@lang('auth.password')</label>

                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="password-confirm">@lang('auth.passwordConfirm')</label>

                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>

                        <div class="form-check text-center">
                            <input class="form-check-input" type="checkbox" id="pp" name="pp" required>

                            <label class="form-check-label" for="tac">
                                @lang('auth.read') <a href="{{ route('pp') }}" target="_blank">@lang('layout.footer.pp')</a>
                            </label>
                        </div>

                        <button type="submit" class="btn btn-first btn-block mt-3">@lang('auth.register')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
