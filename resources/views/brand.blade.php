@extends('layouts.public')

@section('title', 'Brand - ' . config('app.name'))

@section('content')
    <div id="page-content">
        <div class="container pt-2 pt-lg-5 pb-5">
            <h1>@lang('brand.title')</h1>

            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <p class="text-center big-icon mb-0">@fa('tshirt')</p>

                    <p class="text-center lead"><span class="h2 color-first font-primary">For This Shi(r)t</span><br> @lang('brand.1')</p>

                    <hr class="part first">

                    <p>@lang('brand.2')</p>

                    <div class="row">
                        <div class="col-12 col-lg-6">
                            <img src="{{ mix('images/tshirts/old/1_800x600.jpg') }}" class="img-fluid d-block mx-auto img-thumbnail mb-2 mb-lg-0" alt="">
                        </div>

                        <div class="col-12 col-lg-6">
                            <img src="{{ mix('images/tshirts/sober/1_800x600.jpg') }}" class="img-fluid d-block mx-auto img-thumbnail" alt="">
                        </div>
                    </div>

                    <p class="mt-2">@lang('brand.3')</p>

                    <div class="text-center pb-2">
                        <a href="{{ route('products.index') }}" class="btn btn-first btn-lg">@lang('products/index.all')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection