@extends('layouts.public')

@section('title', 'Claims & returns - ' . config('app.name'))

@section('content')
    <div id="page-content">
        <div class="container pt-2 pt-lg-5 pb-5">
            <h1>Claims & returns</h1>

            <p>Last updated: 16.9.2019</p>

            <h2 class="color-first">Complaint conditions</h2>

            <ol>
                <li>The customer must visually inspect the goods immediately upon receipt. He shall be entitled not to accept the goods if there is visible mechanical damage obviously due to the transport or if the goods are incomplete. If the complaint is made within 7 days from the date of delivery, Yasmin Trade s.r.o. undertakes to replace the damaged goods. Following the possible recurrence of this case, Yasmin Trade s.r.o. allows the customer to cancel the order. Delayed claims of this type will not be Yasmin Trade s.r.o. accept and will deal with them as a normal claim referred to in point 2.</li>

                <li>If a claim is made later, the usual claim procedure set out in Yasmin Trade s.r.o. In the event of a claim, the customer may contact Yasmin Trade s.r.o. by telephone (+421 911 415 992), by e-mail (fts@yasmin-trade.com) or by post. The place of complaint is the registered office of Yasmin Trade s.r.o. (Na vŕšku 6, 811 01, Bratislava, Slovak Republic). When making a claim, the customer is obliged to deliver the claimed goods in the original packaging, including a copy of the delivery note or invoice. All goods sold are covered by the statutory warranty period, which is specified in the warranty card delivered with the goods, unless otherwise stated for the goods.</li>

                <li>After receiving the goods together with the complaint we are obliged within 30 days in writing by e-mail, respectively. registered letter to inform you about the result of the complaint procedure.</li>

                <li>If the claim is recognized, you have the right to exchange the defective goods for a new one or a refund.</li>

                <li>In case of unjustified complaint, the customer is obliged to pay the costs incurred in resolving this complaint.</li>

                <li>The warranty becomes void if the defect was caused by mechanical damage to the product (misuse) on the buyer's side.</li>
            </ol>

            <p>If the claim is justified, the buyer has the right to:</p>

            <ul>
                <li>to replace the product for a new one or to withdraw from the contract (refund the purchase price by credit note) if it is an irremovable defect or removable defects repeatedly irrespective of their type and prevents the product from being used for its intended purpose (the buyer has the right choose between replacing a product or returning the purchase price - it can't change its choice subsequently)</li>

                <li>in the event of a replacement, the warranty period shall commence again upon receipt of the new product</li>
            </ul>

            <h2 class="color-first">Returns within 14 days</h2>

            <ul>
                <li>The customer of the e-shop has the right to withdraw from the order without giving a reason within 14 days of receipt of the goods. If you purchase the goods in connection with your business (the invoice includes the identification number), the right of withdrawal does not arise.</li>

                <li>Goods can be returned personally in our store or mailed to Yasmin Trade s.r.o., Na vŕšku 6, 811 01, Bratislava, Slovak Republic.</li>

                <li>The returned goods must be unused, in their original condition and packaging as if they were taken over. Proof of purchase is also required.</li>

                <li>Do not send the goods cash on delivery, as such shipment will not be received. We recommend sending the goods by registered mail and insuring it.</li>

                <li>Please tell us at fts@yasmin-trade.com that you are shipping the goods back - please include your order number.</li>

                <li>Upon receipt of the goods, we will inspect it and inform you by e-mail about the expected date of equipment.</li>
            </ul>
        </div>
    </div>
@endsection