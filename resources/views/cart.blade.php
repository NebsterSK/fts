@extends('layouts.public')

@section('title', 'Cart - ' . config('app.name'))

@section('content')
    <div id="page-content">
        <div class="container pt-2 pt-lg-5 pb-5">
            <h1>@lang('cart.title')</h1>

            @if($objCart->count() == 0)
                <div class="text-center">
                    <p class="big-icon">@fa('shopping-cart')</p>

                    <p class="lead">@lang('cart.empty')</p>

                    <a href="{{ route('products.index') }}" class="btn btn-first btn-lg">@lang('products/index.all')</a>
                </div>
            @else
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>@lang('cart.product')</th>

                        <th class="text-right">@lang('cart.price')</th>

                        <th class="text-right">@lang('cart.quantity')</th>

                        <th class="text-right">@lang('cart.subtotal')</th>

                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($objCart as $objItem)
                        <tr>
                            <td>
                                <strong>{{ $objItem->name }}</strong>
                                @if($objItem->options->color != ''), <span class="text-capitalize">{{ $objItem->options->color }}</span>@endif
                                , <span class="text-uppercase">{{ $objItem->options->size }}</span>
                            </td>

                            <td class="text-right">€ {{ number_format($objItem->price, 2, ',', ' ') }}</td>

                            <td class="text-right">{{ $objItem->qty }}</td>

                            <td class="text-right">€ {{ number_format($objItem->subtotal, 2, ',', ' ') }}</td>

                            <td class="text-right"><a href="{{ route('cart.remove', ['id' => $objItem->rowId]) }}" class="text-danger">@fa('times')</a>
                            </td>
                        </tr>
                    @endforeach

                    <tr>
                        <td colspan="3" class="text-right">@lang('cart.shipping')</td>

                        <td class="text-right">€ 1,00</td>

                        <td></td>
                    </tr>

                    <tr class="lead">
                        <td colspan="3" class="text-right">@lang('cart.total')</td>

                        <td class="text-right">€ {{ number_format(Cart::subtotalFloat() + 1, 2, ',', ' ') }}</td>

                        <td></td>
                    </tr>
                    </tbody>
                </table>

                <div class="text-right">
                    <a href="{{ route('products.index') }}" class="btn btn-link btn-lg">@lang('cart.continue')</a>

                    <a href="{{ route('orders.create') }}" class="btn btn-first btn-lg">@lang('cart.create')</a>
                </div>
            @endif
        </div>
    </div>
@endsection