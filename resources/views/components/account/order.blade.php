<tr>
    <td>{{ $objOrder->created_at->format('j.n. H:i') }}</td>

    <td>{{ $objOrder->id }}</td>

    <td class="text-right">&euro; {{ number_format($objOrder->total, 2, ',', ' ') }}</td>

    <td>
        @if($objOrder->status == 'sent')
            <span class="text-primary">@fa('check-double')</span> Sent
        @elseif($objOrder->status == 'paid')
            <span class="text-success">@fa('check')</span> Paid
        @else
            @fa('hourglass') {{ ucfirst($objOrder->status) }}
        @endif
    </td>

    <td class="text-right"><a href="{{ route('account.orders.show', ['id' => $objOrder->id]) }}" class="btn btn-first btn-sm">Detail</a></td>
</tr>