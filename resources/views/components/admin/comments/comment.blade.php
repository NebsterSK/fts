<div>
    <hr>

    <p class="mb-0"><small>{{ $objComment->created_at->format('j.n. H:i') }} by {{ $objComment->author->name }}</small></p>

    <p>{{ $objComment->text }}</p>
</div>
