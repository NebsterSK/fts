<tr>
    <td class="text-nowrap">{{ $objOrder->created_at->format('j.n. H:i') }}</td>

    <td class="text-nowrap">
        @if($objOrder->status == 'sent')
            <span class="text-primary">@fa('check-double')</span> Sent
        @elseif($objOrder->status == 'paid')
            <span class="text-success">@fa('check')</span> Paid
        @else
            @fa('hourglass') {{ ucfirst($objOrder->status) }}
        @endif
    </td>

    <td class="text-nowrap">
        @if($objOrder->user_id)
            <a href="{{ route('admin.users.show', ['id' => $objOrder->user_id]) }}">{{ $objOrder->full_name }}</a>
        @else
            {{ $objOrder->full_name }}
        @endif
    </td>

    <td>{{ $objOrder->address }}</td>

    <td><a href="mailto:{{ $objOrder->email }}">{{ $objOrder->email }}</a></td>

    <td>{{ $objOrder->phone }}</td>

    <td class="text-right">&euro; {{ number_format($objOrder->total, 2, ',', ' ') }}</td>

    <td class="text-right"><a href="{{ route('admin.orders.show', ['id' => $objOrder->id]) }}" class="btn btn-first btn-sm">Detail</a></td>
</tr>