<tr>
    <td>{{ $objItem->product->name }}</td>

    <td class="text-capitalize">{{ $objItem->colorName }}</td>

    <td class="text-uppercase">{{ $objItem->size->name }}</td>

    <td>{{ $objItem->amount }}</td>

    <td class="text-right">&euro; {{ number_format($objItem->product->price, 2, ',', ' ') }}</td>

    <td class="text-right">&euro; {{ number_format($objItem->amount * $objItem->product->price, 2, ',', ' ') }}</td>
</tr>