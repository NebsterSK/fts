@if($status == 'created')
    <option value="created" selected>Created</option>
@else
    <option value="created">Created</option>
@endif
@if($status == 'paid')
    <option value="paid" selected>Paid</option>
@else
    <option value="paid">Paid</option>
@endif
@if($status == 'sent')
    <option value="sent" selected>Sent</option>
@else
    <option value="sent">Sent</option>
@endif