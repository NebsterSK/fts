<tr>
    <td>{{ $objProduct->product->name }}</td>

    <td class="text-capitalize">{{ $objProduct->colorName }}</td>

    <td class="text-uppercase">{{ $objProduct->size->name }}</td>

    <td class="text-right">{{ $objProduct->sum }}</td>

    <td class="text-right">{{ number_format($objProduct->perc, 2, ',', ' ') }} %</td>
</tr>