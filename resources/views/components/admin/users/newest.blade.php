<tr>
    <td class="text-nowrap">{{ $objUser->created_at->format('j.n. H:i') }}</td>

    <td class="text-nowrap"><a href="{{ route('admin.users.show', ['id' => $objUser->id]) }}">{{ $objUser->full_name }}</a></td>

    <td><a href="mailto:{{ $objUser->email }}">{{ $objUser->email }}</a></td>
</tr>