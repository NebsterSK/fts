<tr>
    <td>{{ $objOrder->id }}</td>

    <td>{{ $objOrder->created_at->format('j.n. H:i') }}</td>

    <td>{{ $objOrder->status }}</td>

    <td>{{ $objOrder->address }}</td>

    <td class="text-right">&euro; {{ number_format($objOrder->total, 2, ',', ' ') }}</td>

    <td class="text-right"><a href="{{ route('admin.orders.show', ['id' => $objOrder->id]) }}" class="btn btn-first btn-sm">Detail</a></td>
</tr>