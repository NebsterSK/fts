<tr>
    <td>{{ $objUser->id }}</td>

    <td><a href="{{ route('admin.users.show', ['id' => $objUser->id]) }}">{{ $objUser->full_name }}</a></td>

    <td><a href="mailto:{{ $objUser->email }}">{{ $objUser->email }}</a></td>
</tr>