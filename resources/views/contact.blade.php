@extends('layouts.public')

@section('addCSS')
    {!! htmlScriptTagJsApi() !!}
@endsection

@section('title', 'Contact - ' . config('app.name'))

@section('content')
    <div id="page-content">
        <div class="container pt-2 pt-lg-5 pb-5">
            <h1>@lang('contact.title')</h1>

            <div class="row">
                <div class="col-12 col-lg-6">
                    <img src="https://yasmin-trade.com/images/logos/160x160.png" class="img-fluid d-block mx-auto" alt="Logo">

                    <p class="text-center color-green h2 mt-1">Yasmin Trade s.r.o.</p>

                    <address>
                        Na vŕšku 6

                        <br>

                        811 01, Bratislava

                        <br>

                        Slovak Republic
                    </address>

                    <address>
                        @lang('contact.companyId'): 51 333 147
                    </address>

                    <hr>

                    <address>
                        <strong>@lang('contact.oh')</strong>
                    </address>

                    <address>
                        @fa('envelope') <a href="mailto:fts@yasmin-trade.com">fts@yasmin-trade.com</a>

                        <br>

                        @fa('phone') <a href="tel:+421911415992">+421 911 415 992</a>
                    </address>
                </div>

                <div class="col-12 col-lg-6 pt-5 pt-lg-0">
                    @include('includes/errors')

                    <form action="{{ route('contactPost') }}" method="POST">
                        @csrf

                        <div class="form-group">
                            <label for="name">@lang('contact.name')</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="@lang('contact.name')" maxlength="100" required>
                        </div>

                        <div class="form-group">
                            <label for="email">@lang('contact.email')</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="@lang('contact.email')" maxlength="100" required>
                        </div>

                        <div class="form-group">
                            <label for="message">@lang('contact.message')</label>
                            <textarea class="form-control" id="message" name="message" placeholder="@lang('contact.message')" maxlength="300" rows="4" required></textarea>
                        </div>

                        <div class="recaptcha">
                            {!! htmlFormSnippet() !!}
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-lg btn-first">@lang('contact.send')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection