@component('mail::message')
# Contact form

**Name:** {{ $arrData['name'] }}

**E-mail:** {{ $arrData['email'] }}

**Message:** {{ $arrData['message'] }}
@endcomponent