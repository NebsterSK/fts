@component('mail::message')
# Hi {{ $objOrder->name }}!

Your order at forthisshirt.com was **created**. Below you can find a recap and URL to Payment page.

Invoice will be generated and send to you in next e-mail after successful payment.

@component('mail::table')
    | Product       | Amount        | Price    |
    | ------------- |--------------:| --------:|
	@foreach($objOrder->ordered_products as $objProduct)
    | {{ $objProduct->formattedName }}      | {{ $objProduct->amount }}      |  € {{ number_format($objProduct->product->price, 2, ',', ' ') }}      |
	@endforeach
	| |Shipping|€ 1,00|
	| |Total (with VAT)|€ {{ number_format($objOrder->total, 2, ',', ' ') }}|
@endcomponent

@component('mail::button', ['url' => route('orders.payment', ['order' => $objOrder->id]), 'color' => 'first'])
	Payment
@endcomponent

Team **{{ config('app.name') }}**
@endcomponent