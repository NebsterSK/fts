@component('mail::message')
#Order #{{ $objOrder->id }} - {{ $objOrder->created_at->format('j.n.Y H:i:s') }}

**Customer:**<br>
{{ $objOrder->full_name }}<br>
{{ $objOrder->street }}<br>
{{ $objOrder->zip }}, {{ $objOrder->city }}<br>
{{ $objOrder->country }}

**Note:** {{ $objOrder->note }}

@component('mail::table')
    | Product       | Amount        | Price    |
    | ------------- |--------------:| --------:|
	@foreach($objOrder->ordered_products as $objProduct)
    | {{ $objProduct->formattedName }}      | {{ $objProduct->amount }}      |  € {{ number_format($objProduct->product->price, 2, ',', ' ') }}      |
	@endforeach
	| |Shipping|€ 1,00|
	| |Total (with VAT)|€ {{ number_format($objOrder->total, 2, ',', ' ') }}|
@endcomponent

@endcomponent