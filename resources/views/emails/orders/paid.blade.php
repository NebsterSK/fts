@component('mail::message')
# Hi {{ $objOrder->name }}!

Thank you very much for supporting us by buying For This Shi(r)t product!

Your order at forthisshirt.com was **paid**. The invoice is in the attachment of this e-mail. You can also find it anytime by logging in on our website.

@component('mail::table')
    | Product       | Amount        | Price    |
    | ------------- |--------------:| --------:|
	@foreach($objOrder->ordered_products as $objProduct)
    | {{ $objProduct->formattedName }}      | {{ $objProduct->amount }}      |  € {{ number_format($objProduct->product->price, 2, ',', ' ') }}      |
	@endforeach
	| |Shipping|€ 1,00|
	| |Total (with VAT)|€ {{ number_format($objOrder->total, 2, ',', ' ') }}|
@endcomponent

We will pack and send your order as soon as possible. Usually the same or next working day.

Team **{{ config('app.name') }}**
@endcomponent