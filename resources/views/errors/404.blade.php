@extends('layouts/public')

@section('title', 'Page not found - ' . config('app.name'))

@section('navigation')@endsection

@section('content')
    <div id="page-content">
        <div class="container text-center pt-2 pt-lg-5 pb-5">
            <img src="{{ mix('images/logos/250x250.png') }}" class="img-fluid d-block mx-auto mb-2 mb-lg-5" alt="">

            <h1>Page not found</h1>

            <p>The page you are looking for doesn't exist.</p>

            <hr>

            <a href="{{ route('index') }}" class="btn btn-first btn-lg">Go to Homepage</a>
        </div>
    </div>
@endsection

@section('footer')@endsection
