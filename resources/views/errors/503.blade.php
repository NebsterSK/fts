@extends('layouts/public')

@section('title', config('app.name'))

@section('addCSS')
    <meta http-equiv="refresh" content="300">
@endsection

@section('navigation')@endsection

@section('content')
    <div id="page-content">
        <div class="container text-center pt-2 pt-lg-5 pb-5">
            <img src="{{ mix('images/logos/250x250.png') }}" class="img-fluid d-block mx-auto mb-2 mb-lg-5" alt="">

            <h1>For This Shirt</h1>

            <p>Website is currently undergoing update or maintenance.</p>

            <p>This should not take longer than 5 minutes. We will be back up soon.</p>
        </div>
    </div>
@endsection

@section('footer')@endsection
