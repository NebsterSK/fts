@extends('layouts.public')

@section('title', 'FAQ - ' . config('app.name'))

@section('content')
    <div id="page-content">
        <div class="container pt-2 pt-lg-5 pb-5">
            <h1>@lang('faq.title')</h1>

            <div class="row">
                <div class="col-12 col-lg-6">
                    <p class="text-center big-icon mb-3">@fa('question')</p>

                    <h3 class="color-first">@lang('faq.1')</h3>

                    <p>@lang('faq.1a')</p>

                    <hr class="part">

                    <h3 class="color-first">@lang('faq.2')</h3>

                    <p>@lang('faq.2a')</p>

                    <hr class="part">

                    <h3 class="color-first">@lang('faq.3')</h3>

                    <p>@lang('faq.3a')</p>

                    <hr class="part">

                    <h3 class="color-first">@lang('faq.4')</h3>

                    <p>@lang('faq.4a')</p>

                    <hr class="part">

                    <h3 class="color-first">@lang('faq.5')</h3>

                    <p>@lang('faq.5a')</p>
                </div>

                <div class="col-12 col-lg-6">
                    <p class="text-center big-icon mb-3">@fa('lock')</p>

                    <h3 class="color-first">@lang('faq.6')</h3>

                    <p>@lang('faq.6a')</p>

                    <p><strong><a href="https://www.paypal.com" target="_blank">PayPal</a> @lang('faq.6a4')</strong> @lang('faq.6a2')</p>

                    <p>@lang('faq.6a3') <a href="{{ route('pp') }}">@lang('layout.footer.pp')</a>.</p>
                </div>
            </div>
        </div>
    </div>
@endsection