<div class="alert alert-success mt-3 mb-0 p-2">
    <p class="text-center mb-0">@fa('check') @lang('products/show.1') <span data-toggle="tooltip" data-placement="top" title="@lang('products/show.2')">@fa('info-circle')</span></p>

    <p class="text-center mb-0">@fa('check') @lang('products/show.3') <span data-toggle="tooltip" data-placement="top" title="@lang('products/show.4')">@fa('info-circle')</span></p>

    <p class="text-center mb-0">@fa('check') @lang('products/show.5')</p>
</div>