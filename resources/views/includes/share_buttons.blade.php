<div id="share-buttons" class="float-right">
    <a href="https://www.facebook.com/sharer/sharer.php?u={{ URL::current() }}" class="btn btn-fb d-block d-lg-inline-block" target="_blank">@fa('facebook') Share</a>

    <a href="https://twitter.com/intent/tweet?url={{ URL::current() }}" class="btn btn-tw d-block d-lg-inline-block" target="_blank">@fa('twitter') Tweet</a>
</div>