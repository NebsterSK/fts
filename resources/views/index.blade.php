@extends('layouts.public')

@section('content')
    <div id="page-content">
        <div class="container-fluid">
            <div id="indexPresenter" class="row">
                <div class="col-12 p-0">
                    <div id="mainCarousel" class="carousel slide carousel-fade" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <picture>
                                    <source media="(min-width: 992px)" srcset="{{ mix('images/carousel/3.jpg') }}">
                                    <img src="{{ mix('images/carousel/3s.jpg') }}" class="d-block w-100" alt="">
                                </picture>
                            </div>

                            <div class="carousel-item">
                                <picture>
                                    <source media="(min-width: 992px)" srcset="{{ mix('images/carousel/2.jpg') }}">
                                    <img src="{{ mix('images/carousel/2s.jpg') }}" class="d-block w-100" alt="">
                                </picture>
                            </div>

                            <div class="carousel-item">
                                <picture>
                                    <source media="(min-width: 992px)" srcset="{{ mix('images/carousel/1.jpg') }}">
                                    <img src="{{ mix('images/carousel/1s.jpg') }}" class="d-block w-100" alt="">
                                </picture>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6 p-0">
                    <div id="old" class="product">
                        <div class="content">
                            <h2 class="font-primary color-first text-shadow mb-0">OLD</h2>

                            <p class="lead d-none d-md-block">I am too OLD For This Shit</p>

                            <a href="{{ route('products.old', ['white', 'm']) }}" class="btn btn-second btn-lg">@lang('index.btn')</a>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6 p-0">
                    <div id="sober" class="product">
                        <div class="content">
                            <h2 class="font-primary color-second text-shadow mb-0">SOBER</h2>

                            <p class="lead d-none d-md-block">I am too SOBER For This Shit</p>

                            <a href="{{ route('products.sober', ['black', 'm']) }}" class="btn btn-first btn-lg">@lang('index.btn')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="pt-5 pb-5">
                <h3 class="text-center font-primary">@lang('index.quality')</h3>

                <p class="text-center lead">@lang('index.1') <span class="color-first">@lang('index.2')</span> @lang('index.3')<br>@lang('index.4') <span class="color-first">@lang('index.5')</span>.</p>
            </div>
        </div>
    </div>
@endsection