<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    @includeWhen(config('app.env') == 'production', 'includes/codes/gtm')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/logos/32x32.png') }}">
    @section('seo')
        <meta name="keywords" content="t-shirt, shirt, t-shirts, shirts, old, sober, shit, sarcasm, joke, gift, fun, meme">
        <meta name="description" content="FOR THIS SHIT t-shirts made of 100% cotton">
    @show
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="{{ mix('css/xs.css') }}">
    <link rel="stylesheet" href="{{ mix('css/sm.css') }}" media="(min-width:576px)">
    <link rel="stylesheet" href="{{ mix('css/md.css') }}" media="(min-width:768px)">
    <link rel="stylesheet" href="{{ mix('css/lg.css') }}" media="(min-width:992px)">
    <link rel="stylesheet" href="{{ mix('css/xl.css') }}" media="(min-width:1200px)">
    @yield('addCSS')
    @section('og')
        <meta property="og:title" content="For This Shi(r)t" />
        <meta property="og:description" content="Original sarcastic 100% cotton t-shirts for casual wear" />
        <meta property="og:image" content="{{ config('app.url') }}{{ mix('images/og.jpg') }}" />
        <meta property="og:url" content="{{ URL::current() }}" />
        <meta property="og:type" content="website" />
        <meta property="fb:app_id" content="1205701956282577" />
    @show
    <title>@yield('title', config('app.name'))</title>
</head>

<body>
@includeWhen(config('app.env') == 'production', 'includes/codes/gtm2')
@yield('startBody')

@section('navigation')
    <nav id="navbar-primary" class="navbar fixed-top navbar-expand-lg">
        <a class="navbar-brand" href="{{ route('index') }}">
            For This Shirt
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapse-primary" aria-controls="collapse-primary" aria-expanded="false" aria-label="Toggle navigation">
            @fa('bars')
        </button>

        <div class="collapse navbar-collapse" id="collapse-primary">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('brand') }}">@lang('layout.nav.brand')</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('products.index') }}">@lang('layout.nav.products')</a>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">@lang('layout.nav.login')</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">@lang('layout.nav.register')</a>
                    </li>
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            @fa('user') {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('account.index') }}">@fa('user-cog') @lang('layout.nav.account')</a>

                            <div class="dropdown-divider"></div>

                            @if(Auth::user()->admin)
                                <a class="dropdown-item" href="{{ route('admin.index') }}">Admin area</a>
                            @endif

                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                @fa('sign-out-alt') @lang('layout.nav.logout')
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endif

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @fa('globe')
                        @if(App::getLocale() == 'en')English
                        @elseif(App::getLocale() == 'hu')Magyar
                        @elseif(App::getLocale() == 'pl')Polski
                        @elseif(App::getLocale() == 'sk')Slovensky
                        @endif
                    </a>

                    <div class="dropdown-menu languages" aria-labelledby="navbarDropdown">
                        @if(App::getLocale() != 'en')<a class="dropdown-item" href="?lang=en">English</a>@endif

                        @if(App::getLocale() != 'hu')<a class="dropdown-item" href="?lang=hu">Magyar</a>@endif

                        @if(App::getLocale() != 'pl')<a class="dropdown-item" href="?lang=pl">Polski</a>@endif

                        @if(App::getLocale() != 'sk')<a class="dropdown-item" href="?lang=sk">Slovensky</a>@endif
                    </div>
                </li>

                <li class="nav-item action">
                    <a class="nav-link" href="{{ route('cart.index') }}">@fa('shopping-cart') @lang('layout.nav.cart') ({{ Cart::count() }})</a>
                </li>
            </ul>
        </div>
    </nav>
@show

@yield('content')

@section('footer')
    <footer id="page-footer">
        <div class="container">
            <p class="payment-methods mb-0">@fa('cc-paypal') @fa('cc-visa') @fa('cc-mastercard')</p>

            <hr class="part first">

            <p class="social-networks">
                <a href="https://www.facebook.com/forthisshirt/" target="_blank">@fa('facebook')</a>

                <a href="https://www.instagram.com/for_this_shirt/" target="_blank">@fa('instagram')</a>

{{--                <a href="" target="_blank">@fa('instagram')</a>--}}

{{--                <a href="" target="_blank">@fa('twitter')</a>--}}
            </p>

            <p class="mb-0"><a href="{{ route('contact') }}">@lang('layout.nav.contact')</a> | <a href="{{ route('faq') }}">FAQ</a></p>

            <p><a href="{{ route('pp') }}">@lang('layout.footer.pp')</a> | <a href="{{ route('tac') }}">@lang('layout.footer.tac')</a> | <a href="{{ route('car') }}">@lang('layout.footer.car')</a></p>

            <p class="mb-0">Yasmin Trade s.r.o. &copy; {{ date('Y') }}</p>
        </div>
    </footer>
@show

@include('sweetalert::alert')

@yield('endBody')

<script type="text/javascript" src="{{ mix('js/app.js') }}"></script>

@yield('addJS')
</body>
</html>