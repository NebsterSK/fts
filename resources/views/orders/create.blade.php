@extends('layouts/public')

@section('title', 'New order - ' . config('app.name'))

@section('content')
    <div id="page-content">
        <div class="container pt-2 pt-lg-5 pb-2">
            <h1>@lang('orders/create.title') <small class="color-first">@lang('orders/create.title2')</small></h1>

            <h3>@lang('orders/create.products')</h3>

            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>@lang('cart.product')</th>

                    <th class="text-right">@lang('cart.price')</th>

                    <th class="text-right">@lang('cart.quantity')</th>

                    <th class="text-right">@lang('cart.subtotal')</th>
                </tr>
                </thead>

                <tbody>
                @foreach($objCart as $objItem)
                    <tr>
                        <td>
                            <strong>{{ $objItem->name }}</strong>
                            @if($objItem->options->color != ''), <span class="text-capitalize">{{ $objItem->options->color }}</span>@endif
                            , <span class="text-uppercase">{{ $objItem->options->size }}</span>
                        </td>

                        <td class="text-right">€ {{ number_format($objItem->price, 2, ',', ' ') }}</td>

                        <td class="text-right">{{ $objItem->qty }}</td>

                        <td class="text-right">€ {{ number_format($objItem->subtotal, 2, ',', ' ') }}</td>
                    </tr>
                @endforeach

                <tr>
                    <td colspan="3" class="text-right">@lang('cart.shipping')</td>

                    <td class="text-right">€ 1,00</td>
                </tr>

                <tr class="lead">
                    <td colspan="3" class="text-right">@lang('cart.total')</td>

                    <td class="text-right">€ {{ number_format(Cart::subtotalFloat() + 1, 2, ',', ' ') }}</td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="bg-divider pt-2 pb-5">
            <div class="container">
                <h3>@lang('orders/create.delivery')</h3>

                @include('includes.errors')

                <form action="{{ route('orders.store') }}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <label for="name">@lang('auth.fname')</label>

                                <input type="text" class="form-control" id="name" name="name" required maxlength="20">
                            </div>

                            <div class="form-group">
                                <label for="surname">@lang('auth.lname')</label>

                                <input type="text" class="form-control" id="surname" name="surname" required maxlength="30">
                            </div>

                            <div class="form-group">
                                <label for="email">@lang('auth.email')</label>

                                <input type="email" class="form-control" id="email" name="email" required maxlength="100">
                            </div>

                            <div class="form-group">
                                <label for="phone">@lang('auth.phone')</label>

                                <input type="text" class="form-control" id="phone" name="phone" required maxlength="30">
                            </div>
                        </div>

                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <label for="street">@lang('auth.street')</label>

                                <input type="text" class="form-control" id="street" name="street" required maxlength="100">
                            </div>

                            <div class="form-group">
                                <label for="city">@lang('auth.city')</label>

                                <input type="text" class="form-control" id="city" name="city" required maxlength="50">
                            </div>

                            <div class="form-group">
                                <label for="zip">@lang('auth.zip')</label>

                                <input type="text" class="form-control" id="zip" name="zip" required maxlength="20">
                            </div>

                            <div class="form-group">
                                <label for="country">@lang('auth.country')</label>

                                <input type="text" class="form-control" id="country" name="country" required maxlength="50">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="note">@lang('auth.note')</label>

                        <textarea class="form-control" id="note" name="note" maxlength="300" rows="4"></textarea>
                    </div>

                    <hr class="part first">

                    <div class="text-right mt-2">
                        <div class="form-check d-inline-block">
                            <input class="form-check-input" type="checkbox" id="tac" name="tac" required>

                            <label class="form-check-label" for="tac">
                                @lang('orders/create.read') <a href="{{ route('tac') }}" target="_blank">@lang('layout.footer.tac')</a> @lang('orders/create.and') <a href="{{ route('pp') }}" target="_blank">@lang('layout.footer.pp')</a>
                            </label>
                        </div>

                        <button type="submit" class="d-inline-block ml-5 btn btn-first btn-lg">@lang('orders/create.order')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('addJS')
    {{-- Preloads PayPal script --}}
    <script src="https://www.paypal.com/sdk/js?client-id={{ config('paypal.client_id') }}&currency=EUR" async></script>
@endsection