@extends('layouts/public')

@section('title', 'Payment - ' . config('app.name'))

@section('addCSS')
    <script src="https://www.paypal.com/sdk/js?client-id={{ config('paypal.client_id') }}&currency=EUR" data-order-id="{{ $objOrder->id }}"></script>
@endsection

@section('startBody')
    <div class="modal fade" id="redirectModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-center pt-5 pb-5">
                    <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
                        <span class="sr-only">@lang('orders/payment.redirecting')</span>
                    </div>

                    <p class="h3 mt-3 mb-0">@lang('orders/payment.redirecting')</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div id="page-content">
        <div class="container pt-2 pt-lg-5 pb-5">
            <h1>@lang('orders/create.title') <small class="color-first">@lang('orders/payment.title2')</small></h1>

            <div class="row">
                <div class="col-12 col-lg-8">
                    <h3>@lang('orders/create.products')</h3>

                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>@lang('cart.product')</th>

                            <th class="text-right">@lang('cart.price')</th>

                            <th class="text-right">@lang('cart.quantity')</th>

                            <th class="text-right">@lang('cart.subtotal')</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($objOrder->ordered_products as $objItem)
                            <tr>
                                <td>{{ $objItem->formattedName }}</td>

                                <td class="text-right">€ {{ number_format($objItem->product->price, 2, ',', ' ') }}</td>

                                <td class="text-right">{{ $objItem->amount }}</td>

                                <td class="text-right">€ {{ number_format($objItem->product->price * $objItem->amount, 2, ',', ' ') }}</td>
                            </tr>
                        @endforeach

                        <tr>
                            <td colspan="3" class="text-right">@lang('cart.shipping')</td>

                            <td class="text-right">€ 1,00</td>
                        </tr>

                        <tr class="lead">
                            <td colspan="3" class="text-right">@lang('cart.total')</td>

                            <td class="text-right">€ {{ number_format($objOrder->total, 2, ',', ' ') }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-12 col-lg-4">
                    <h3>@lang('orders/payment.methods')</h3>

                    <div class="alert alert-success">
                        <p class="mb-0">@fa('lock') @lang('orders/payment.store')</p>
                    </div>

                    <div id="paypal-button-container"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('addJS')
    <script>
        paypal.Buttons({
            // style: {
            //     layout: 'vertical'
            // },
            // enableStandardCardFields: true,
            createOrder: function(data, actions) {
                return actions.order.create({
                    // intent: 'CAPTURE',
                    payer: {
                        name: {
                            given_name: '{{ $objOrder->name }}',
                            surname: '{{ $objOrder->surname }}'
                        },
                        email_address: '{{ $objOrder->email }}'
                    },
                    purchase_units: [{
                        amount: {
                            value: '{{ $objOrder->total }}'
                        }
                    }]
                });
            },
            onApprove: function(data, actions) {
                return actions.order.capture().then(function(details) {
                    $('#redirectModal').modal();

                    window.location.replace('/orders/' + {{ $objOrder->id }} + '/pay/' + data.orderID);
                });
            }
        }).render('#paypal-button-container');
    </script>
@endsection
