@extends('layouts.public')

@section('title', 'Privacy policy - ' . config('app.name'))

@section('content')
    <div id="page-content">
        <div class="container pt-2 pt-lg-5 pb-5">
            <h1>Privacy policy</h1>

            <p>Last updated: 17.9.2019</p>

            <h2 class="color-first">1. Identification information of the online store operator forthisshirt.com</h2>

            <p>The operator of the online store forthisshirt.com is the company Yasmin Trade sro, with its registered office at Na vŕšku 6, 811 01 Bratislava, IČO 51 333 147, registered in the Commercial Register of the District Court Bratislava I, section Sro, insert number 125893 / B, e-mail: fts@yasmin-trade.com, tel. no. +421 911 415 992, which processes personal data under the conditions below.</p>

            <h2 class="color-first">2. The scope of the personal data processed, the purposes for which the personal data are intended and the legal basis for the processing</h2>

            <p>Yasmin Trade s.r.o. respects your privacy. Our goal is to offer you full service, so we need to know some of your personal information. We protect this data from misuse and we guarantee that it will not be disclosed to a third party except for the recipients as defined in point 3 below. This applies to your identification and contact information, which we need in order to conclude and fulfill the order, respectively. for direct marketing, and your purchase data. The provision of the personal data concerned constitutes a binding order. You are not required to provide us with this personal information, but if it is not provided to the full extent described below, we will not be able to send you an order or your personal information. to its proper fulfillment by Yasmin Trade s.r.o.</p>

            <p>In the case of natural persons, these are the following personal data: name and surname, home address, resp. delivery address, phone number and email address. <!--In the case of legal entities or sole traders, these are the following information: business name, address of the registered office or place of business, company ID, VAT number, VAT ID and email address.--></p>

            <p>If you have provided us with personal information, it will only be used for the purpose of:</p>

            <ul>
                <li>properly providing our services to customers, to fulfill the order placed through the online shop between us and you as a customer, before confirming this order to answer your questions regarding the provision of our services and the technical administration necessary for the proper provision of our services at forthisshirt.com, all on the legal basis of the Commercial Code of the Slovak Republic and with your consent.</li>

                <li>marketing purposes if you have registered with forthisshirt.com on the legal basis of your consent as expressed by your registration.</li>
            </ul>

            <h2 class="color-first">3. Recipients of personal data</h2>

            <p>Your personal information may be provided to a third party if it is necessary to complete the order or if you have given your explicit consent to such provision. To process your orders we need your name, surname, address (home / delivery), e-mail and phone number. Your e-mail address serves both as an identifier (for logging in to the customer account) and as a means of communication. As part of order processing, the data is provided to our contracted service providers, such as logistics service provider, transporter, banks, etc.</p>

            <h2 class="color-first">4. Criteria for determining the retention period of personal data</h2>

            <p>We only process your personal data for the duration of the purpose for which we collected your personal data.</p>

            <h2 class="color-first">5. Customer's privacy rights</h2>

            <p>As a data subject, Customer has the right to:</p>

            <ul>
                <li>the right of the operator to request access to his personal data pursuant to Art. 15 of Regulation (EU) 2016/679 of the European Parliament and of the Council of 27 April 2016 on the protection of individuals with regard to the processing of personal data and on the free movement of such data and repealing Directive 95/46 / EC (General Data Protection Regulation); of Act no. 18/2018 Coll. on the protection of personal data and on amendments to certain acts (hereinafter referred to as the “Act”);</li>

                <li>the right to rectify personal data by the controller pursuant to Art. 16 of the General Data Protection Regulation and Section 22 of the Act;</li>

                <li>the right to delete personal data within the meaning of Art. 17 of the General Data Protection Regulation and § 23 of the Act;</li>

                <li>the right to limit the processing of personal data within the meaning of Art. 18 of the General Data Protection Regulation and § 24 of the Act;</li>

                <li>the right to object to the processing of personal data by the controller for the purposes of direct marketing, including profiling, in so far as it relates to direct marketing. If a customer objects to the processing of personal data for direct marketing purposes, the controller may not further process the customer's personal data for direct marketing purposes;</li>

                <li>the right to the transfer of the customer's personal data pursuant to Art. 20 of the General Data Protection Regulation and § 26 of the Act;</li>

                <li>the right to revoke consent to the processing of personal data at any time;</li>

                <li>the right to file a petition for the initiation of proceedings with the Office for Personal Data Protection of the Slovak Republic if the customer believes that he / she was directly affected by his / her rights under the General Data Protection Regulation or under the Act.</li>
            </ul>

            <p>These rights can be exercised by the customer at any time</p>

            <ul>
                <li>in writing by sending a letter to the address of the operator's registered office;</li>

                <li>electronically by sending an e-mail to fts@yasmin-trade.com; or</li>

                <li>by telephone at +421 911 415 992</li>
            </ul>

            <h2 class="color-first">6. Cookies</h2>

            <p>Yasmin Trade s.r.o. ("us", "we", or "our") uses cookies on yasmin-trade.com (the
                "Service"). By using the Service, you consent to the use of cookies.</p>

            <p>Our Cookies Policy explains what cookies are, how we use cookies, how third­parties we may
                partner with may use cookies on the Service, your choices regarding cookies and further information
                about cookies.</p>

            <h3>What are cookies</h3>

            <p>Cookies are small pieces of text sent by your web browser by a website you visit. A cookie file is
                stored in your web browser and allows the Service or a third­party to recognize you and make your
                next visit easier and the Service more useful to you.</p>            <p>Cookies can be "persistent" or "session" cookies.</p>

            <h3>How we use cookies</h3>

            <p>When you use and access the Service, we may place a number of cookies files in your web
                browser.</p>

            <p>We use cookies for the following purposes: to enable certain functions of the Service, to provide
                analytics, to store your preferences, to enable advertisements delivery, including behavioral
                advertising.</p>

            <p>We use both session and persistent cookies on the Service and we use different types of cookies to
                run the Service:</p>

            <p>­ Essential cookies. We may use essential cookies to authenticate users and prevent fraudulent use
                of user accounts.</p>

            <p>For the full disclosure section, including all types of cookies (from essentials to advertising
                cookies), create your own Cookies Policy.</p>

            <h3>Third­party cookies</h3>

            <p>In addition to our own cookies, we may also use various third­parties cookies to report usage
                statistics of the Service, deliver advertisements on and through the Service, and so on.</p>

            <p>We use the following third party services: <strong>Google Tag Manager, Google Analytics, Facebook Pixel.</strong></p>

            <h3>What are your choices regarding cookies</h3>

            <p>If you'd like to delete cookies or instruct your web browser to delete or refuse cookies, please visit
                the help pages of your web browser.</p>

            <p>Please note, however, that if you delete cookies or refuse to accept them, you might not be able to
                use all of the features we offer, you may not be able to store your preferences, and some of our
                pages might not display properly.</p>

            <h3>Where can your find more information about cookies</h3>

            <p>You can learn more about cookies and the following third­party websites:</p>

            <ul>
                <li>AllAboutCookies: <a href="http://www.allaboutcookies.org/" target="_blank">http://www.allaboutcookies.org/</a></li>

                <li>Network Advertising Initiative: <a href="https://www.networkadvertising.org/" target="_blank">https://www.networkadvertising.org/</a></li>
            </ul>
        </div>
    </div>
@endsection