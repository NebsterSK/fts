@extends('layouts.public')

@section('title', 'Products - ' . config('app.name'))

@section('content')
    <div id="page-content">
        <div class="container pt-2 pt-lg-5 pb-5">
            <nav class="d-none d-md-block" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('index') }}">@lang('products/index.home')</a></li>

                    <li class="breadcrumb-item active">@lang('products/index.title')</li>
                </ol>
            </nav>

            <h1>@lang('products/index.title')</h1>

            <div class="row">
                <div class="col-12 col-md-6 col-lg-4 pb-3">
                    <div class="card">
                        <img src="{{ mix('images/tshirts/old/800x600.jpg') }}" class="card-img-top" alt="">

                        <div class="card-body">
                            <p class="h4 mb-0 float-right text-right">
                                &euro; 11,95<br>
                                <span class="badge badge-success text-uppercase">@lang('products/index.inStock')</span>
                            </p>

                            <h3 class="card-title font-primary color-second">T-shirt OLD</h3>

                            <p class="card-text mb-0">@lang('products/index.colors'): <span class="color color-white" data-toggle="tooltip" data-placement="top" title="@lang('products/index.white')"></span></p>

                            <p class="card-text mb-0">@lang('products/index.sizes'): <strong>S, M, L</strong></p>
                        </div>

                        <footer class="card-footer text-right">
                            <a href="{{ route('products.old', ['white', 'm']) }}" class="btn btn-first btn-block">@lang('products/index.details')</a>
                        </footer>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4 pb-3">
                    <div class="card">
                        <img src="{{ mix('images/tshirts/sober/800x600.jpg') }}" class="card-img-top" alt="">

                        <div class="card-body">
                            <p class="h4 mb-0 float-right text-right">
                                &euro; 11,95<br>
                                <span class="badge badge-success text-uppercase">@lang('products/index.inStock')</span>
                            </p>

                            <h3 class="card-title font-primary color-second">T-shirt SOBER</h3>

                            <p class="card-text mb-0">@lang('products/index.colors'): <span class="color color-black" data-toggle="tooltip" data-placement="top" title="@lang('products/index.black')"></span></p>

                            <p class="card-text mb-0">@lang('products/index.sizes'): <strong>S, M, L</strong></p>
                        </div>

                        <footer class="card-footer text-right">
                            <a href="{{ route('products.sober', ['black', 'm']) }}" class="btn btn-first btn-block">@lang('products/index.details')</a>
                        </footer>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card">
                        <img src="{{ mix('images/tshirts/set/800x600.jpg') }}" class="card-img-top" alt="">

                        <div class="card-body">
                            <p class="h4 mb-0 float-right text-right">
                                &euro; 20,32<br>
                                <span class="text-danger"><small>(@lang('products/index.off', ['percent' => 15]))</small></span><br>
                                <span class="badge badge-success text-uppercase">@lang('products/index.inStock')</span>
                            </p>

                            <h3 class="card-title font-primary color-second">T-shirts set</h3>


                            <p class="card-text mb-0">@lang('products/index.colors'): <span class="color color-white" data-toggle="tooltip" data-placement="top" title="@lang('products/index.white')"></span> + <span class="color color-black" data-toggle="tooltip" data-placement="top" title="@lang('products/index.black')"></span></p>

                            <p class="card-text mb-0">@lang('products/index.sizes'): <strong>S, M, L</strong></p>

                        </div>

                        <footer class="card-footer text-right">
                            <a href="{{ route('products.set', ['m']) }}" class="btn btn-first btn-block">@lang('products/index.details')</a>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection