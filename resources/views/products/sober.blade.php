@extends('layouts.public')

@section('og')
    <meta property="og:title" content="I AM TOO SOBER FOR THIS SHIT t-shirt" />
    <meta property="og:description" content="Original sarcastic 100% cotton t-shirt for casual wear" />
    <meta property="og:image" content="{{ config('app.url') }}{{ mix('images/tshirts/sober/og.jpg') }}" />
    <meta property="og:url" content="{{ URL::current() }}" />
    <meta property="og:type" content="website" />
    <meta property="fb:app_id" content="1205701956282577" />
@endsection

@section('title', 'T-shirt SOBER - ' . config('app.name'))

@section('seo')
    <meta name="keywords" content="t-shirt, shirt, sober, sarcasm, joke, gift, fun, meme">
    <meta name="description" content="I AM TOO SOBER FOR THIS SHI(R)T t-shirt made of 100% cotton">
@endsection

@section('content')
    <div id="page-content">
        <div id="tshirt-sober" class="product-bg">
            <div class="container">
                <nav class="d-none d-md-block" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('index') }}">@lang('products/index.home')</a></li>

                        <li class="breadcrumb-item"><a href="{{ route('products.index') }}">@lang('products/index.title')</a></li>

                        <li class="breadcrumb-item active">T-shirt SOBER</li>
                    </ol>
                </nav>

                <div class="row">
                    <div class="col-12 col-md-6 pb-2 pb-lg-0">
                        <a href="{{ mix('images/tshirts/sober/1.jpg') }}" data-lightbox="tshirt">
                            <img src="{{ mix('images/tshirts/sober/1_800x600.jpg') }}" class="img-fluid d-block mx-auto img-thumbnail" alt="">
                        </a>

                        <div class="row mt-2 mt-lg-4">
                            <div class="col-6">
                                <a href="{{ mix('images/tshirts/sober/2.jpg') }}" data-lightbox="tshirt">
                                    <img src="{{ mix('images/tshirts/sober/2s.jpg') }}" class="img-fluid d-block mx-auto img-thumbnail" alt="">
                                </a>
                            </div>

                            <div class="col-6">
                                <a href="{{ mix('images/tshirts/sober/3.jpg') }}" data-lightbox="tshirt">
                                    <img src="{{ mix('images/tshirts/sober/3s.jpg') }}" class="img-fluid d-block mx-auto img-thumbnail" alt="">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="bg-text color-white p-2 p-lg-4">
                            @include('includes/share_buttons')

                            <h1 class="font-primary color-first text-shadow">T-shirt SOBER</h1>

                            <p class="mb-0">
                                <strong>@lang('products/show.gender'):</strong> Unisex

                                <br>

                                <strong>@lang('products/show.material'):</strong> 100 % @lang('products/show.cotton')

                                <br>

                                <strong>@lang('products/show.weight'):</strong> 160 g/m<sup>2</sup>

                                <br>

                                <strong>@lang('products/show.washable'):</strong> 30 °C

                                <br>

                                <span class="badge badge-success">@lang('products/show.inStockLong')</span>
                            </p>

                            <hr>

                            <p class="mb-0">@lang('products/show.size')</p>

                            <div class="list-group list-group-horizontal">
                                <a href="{{ route('products.sober', [$color, 's']) }}" class="list-group-item list-group-item-action @if($size == 's') active @endif">S</a>

                                <a href="{{ route('products.sober', [$color, 'm']) }}" class="list-group-item list-group-item-action @if($size == 'm') active @endif">M</a>

                                <a href="{{ route('products.sober', [$color, 'l']) }}" class="list-group-item list-group-item-action @if($size == 'l') active @endif">L</a>
                            </div>

                            <div class="row mt-3">
                                <div class="col-12 col-sm-5 col-lg-6">
                                    <p class="mb-0">@lang('products/show.price')</p>

                                    <p class="h3 font-weight-bold mb-0 text-shadow">&euro; 11,95</p>
                                </div>

                                <div class="col-12 col-sm-7 col-lg-6">
                                    <form action="{{ route('cart.add') }}" method="POST">
                                        @csrf

                                        <input type="hidden" name="id" value="2">

                                        <input type="hidden" name="color" value="{{ $color }}">

                                        <input type="hidden" name="size" value="{{ $size }}">

                                        <div class="input-group input-group-lg">
                                            <input type="number" class="form-control" name="quantity" value="1" min="1" step="1" max="10" required>

                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-first" id="button-addon2">@fa('shopping-cart') @lang('products/show.buy')</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            @include('includes/features')
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container pt-5 pb-5">
            <h2>@lang('products/show.6')</h2>

            <div class="row">
                <div class="col-12 col-md-8">
                    <table class="table table-striped table-bordered text-center">
                        <thead class="thead-dark">
                        <tr>
                            <th>@lang('products/show.size') (cm)</th>

                            <th>S</th>

                            <th>M</th>

                            <th>L</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td>Height</td>

                            <td>66</td>

                            <td>70</td>

                            <td>74</td>
                        </tr>

                        <tr>
                            <td>Width</td>

                            <td>48</td>

                            <td>52</td>

                            <td>55</td>
                        </tr>
                        </tbody>
                    </table>

                    <p class="text-center color-muted">@lang('products/show.7')</p>
                </div>

                <div class="col-12 col-md-4">
                    <img src="{{ mix('images/tshirts/sizes.jpg') }}" class="img-fluid d-block mx-auto" alt="Sizes">
                </div>
            </div>
        </div>
    </div>
@endsection