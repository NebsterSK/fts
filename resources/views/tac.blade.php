@extends('layouts.public')

@section('title', 'Terms and conditions - ' . config('app.name'))

@section('content')
    <div id="page-content">
        <div class="container pt-2 pt-lg-5 pb-5">
            <h1>Terms & conditions</h1>

            <p>Last updated: 17.9.2019</p>

            <p>Purchases of goods through the e-commerce forthisshirt.com may be made by individuals or business entities without restriction and provided that they comply with the rules below.</p>

            <h2 class="color-first">Binding order</h2>

            <p>The buyer's order is concluded at the moment of delivery of the binding consent of both the buyer and the seller (by binding confirmation of the order by the seller). From this moment, mutual rights and obligations arise between the buyer and the seller.</p>

            <p>By sending a binding order, the buyer confirms that he / she is familiar with these terms and conditions, including the complaint conditions. The buyer is sufficiently informed about these terms and conditions and complaints procedure and has the opportunity to get acquainted with them. The Seller reserves the right to cancel the order or its part before completing the binding order if the goods are no longer manufactured, delivered or the price of the delivered goods has changed significantly. If this happens, the Seller will contact the Buyer immediately to agree on further action. If the buyer has already paid all or part of the purchase price, this amount will be transferred back to his account.</p>

            <p>All orders received by this store are binding. Order can be canceled before shipping. In the event that the order is not canceled prior to shipment and is shipped, the buyer may be required to reimburse the cost of shipping the goods. You are automatically notified by email when you receive an order. If the goods are not in stock or in the supplier's warehouse, we will inform you immediately of the next delivery date.</p>

            <h2 class="color-first">Registration</h2>

            <p>Internet purchase is not subject to registration. Your data will only be used for the internal use of forthisshirt.com and Yasmin Trade s.r.o. declares that they will not be misused for other purposes. In case of registration you will get:</p>

            <ul>
                <li>access information about the status of your orders</li>

                <li>overview of all purchases made so far</li>

                <li>regular information on news and promotions</li>
            </ul>

            <p>The login password can be changed after login. For each subsequent purchase through the e-shop forthisshirt.com you only need to enter your login name and password.</p>

            <h2 class="color-first">Ordering goods</h2>

            <ol>
                <li>After selecting the product, click on the button ADD TO CART and the goods will be placed in the shopping cart. You can choose the number of pieces. You can then continue to create order by clicking CREATE AN ORDER button.</li>

                <li>Fill out the prepared form with identification information, shipping address, and order details. To order your order, click the ORDER AND PAY button. However, you must first tick the checkbox to confirm your understanding of the terms and conditions and privacy policy.</li>

                <li>Once this process is complete, an order with a specific number is generated, under which our automated system records it. If you confirm your order binding, you will immediately receive a confirmation of receipt of the order at the email address you provided in your identification information. Also, if you are logged in, your order will be archived in the section my profile / my orders. At the same time, the business relationship between the customer and Yasmin Trade s.r.o. The entire purchasing process for both parties is subject to the rights and obligations arising from the Complaints Procedure Code, the Civil Code and the Commercial Code.</li>

                <li>The system will generate payment options and after payment of the order you will receive a confirmation of payment together with the invoice to the entered e-mail address.</li>
            </ol>

            <h2 class="color-first">Prices</h2>

            <p>All prices of goods and services are stated including the corresponding VAT (20% - the exact rate can always be found in the order). Stock prices are valid until the date indicated in the stock details or until stocks are sold out, unless otherwise stated. Any additional discounts that are granted in addition to the discounts shown for individual products on our website, individual customer discounts or discounts provided through the cooperation of our online store with third parties are not mutually combinable.</p>

            <h2 class="color-first">Terms of delivery</h2>

            <p>Delivery time depends on the availability of the goods in stock from 2 to 7 working days from the payment of the order.</p>

            <ul>
                <li>All orders are sent by Slovak Post, shipping costs € 2.00.</li>

                <li>If the goods are available in stock, we dispatch them immediately. The shipping date is no later than the next working day.</li>

                <li>If the goods are not available in stock, the delivery time varies from 7 to 30 days. The exact delivery date will be specified by e-mail.</li>

                <li>Yasmin Trade s.r.o. undertakes to deliver the ordered goods to a customer within the European Union no later than 30 days from the date of payment of the order. The invoice will be delivered to the customer by e-mail immediately after payment of the order.</li>

                <li>If the delivery time is unsatisfactory due to technical reasons, we will offer the customer the opportunity to cancel the order. At the same time, the customer is entitled to a full refund.</li>

                <li>Yasmin Trade s.r.o. undertakes to deliver the ordered goods to the customer at the address within the European Union specified by the customer in the order. Transport to the address of destination is provided by the seller.</li>

                <li>The delivery of the subject of performance to the specified place is considered to be the fulfillment of the delivery.</li>

                <li>If the customer repeatedly fails to receive the shipment due to absence or for which Yasmin Trade s.r.o. he / she has not informed in advance, the goods will be re-sent to him at his / her expense upon e-mail contact.</li>

                <li>Yasmin Trade s.r.o. is not responsible for delayed delivery of ordered goods caused by mail or mail. courier service.</li>
            </ul>

            <h2 class="color-first">Proof of purchase</h2>

            <p>You will receive the following documents for the purchased goods:</p>

            <p><strong>Invoice</strong> - You will be issued an invoice for the purchased goods, which contains all the legal requirements. An invoice will be sent to you by e-mail as soon as the order is paid.</p>

            <p><strong>Payment confirmation</strong> - You will receive a confirmation of payment by e-mail as soon as the order is paid together with the invoice.</p>

            <p><strong>Credit note</strong> - We will issue you a credit note if the goods were delivered incorrectly by the seller or the goods were returned from the customer as part of the complaint.</p>
        </div>
    </div>
@endsection