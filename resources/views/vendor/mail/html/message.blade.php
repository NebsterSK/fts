@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ config('app.name') }}
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            <a href="{{ route('index') }}">Website</a> | <a href="{{ route('tac') }}">Terms & conditions</a> | <a href="{{ route('pp') }}">Privacy policy</a><br><br>Yasmin Trade s.r.o. © {{ date('Y') }}
        @endcomponent
    @endslot
@endcomponent
