<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Public
Route::get('/', 'PublicController@index')->name('index');
Route::get('/brand', 'PublicController@brand')->name('brand');
Route::get('/contact', 'PublicController@contact')->name('contact');
Route::post('/contact', 'PublicPostController@contact')->name('contactPost');
Route::get('/faq', 'PublicController@faq')->name('faq');
Route::get('/privacy-policy', 'PublicController@pp')->name('pp');
Route::get('/terms-and-conditions', 'PublicController@tac')->name('tac');
Route::get('/claims-and-returns', 'PublicController@car')->name('car');

// Cart
Route::prefix('cart')->name('cart.')->group(function() {
    Route::get('/', 'CartController@index')->name('index');
    Route::post('/add', 'CartController@add')->name('add');
    Route::get('/remove/{id}', 'CartController@remove')->name('remove');
});

// Orders
Route::resource('orders', 'OrderController')->only([
    'create', 'store'
]);
Route::get('/orders/{order}/payment', 'OrderController@payment')->name('orders.payment');
Route::get('/orders/{order}/pay/{paypal_id}', 'OrderController@pay')->name('orders.pay');
//Route::get('/orders/{order}/gen', 'OrderController@generateInvoice');

// Products
Route::prefix('products')->name('products.')->group(function() {
    Route::get('/', 'ProductController@index')->name('index');
//    Route::get('/t-shirt/{product}/{color}/{size}', 'ProductController@show')->name('product');
    Route::get('/t-shirt/old/{color}/{size}', 'ProductController@old')->name('old');
    Route::get('/t-shirt/sober/{color}/{size}', 'ProductController@sober')->name('sober');
    Route::get('/t-shirt/set/{size}', 'ProductController@set')->name('set');
});

// Auth, Account & Admin
Auth::routes();
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider')->name('fb');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback')->name('fb_callback');

Route::prefix('account')->name('account.')->middleware('auth')->group(function() {
    Route::get('/', 'AccountController@index')->name('index');
    Route::put('/update', 'AccountController@update')->name('update');
    Route::get('/download', 'AccountController@download')->name('download');
    Route::post('/delete', 'AccountController@delete')->name('delete');

    Route::resource('orders', 'OrderController')->only([
        'index', 'show'
    ]);
    Route::get('/orders/{order}/invoice', 'OrderController@invoice')->name('orders.invoice');
});

Route::prefix('admin')->name('admin.')->middleware(['auth', 'check-admin'])->group(function() {
    Route::get('/', 'AdminController@index')->name('index');

    Route::resource('comments', 'Admin\CommentController')->only([
        'store'
    ]);

    Route::resource('orders', 'Admin\OrderController')->only([
        'index', 'show'
    ]);
    Route::get('/orders/{order}/invoice', 'Admin\OrderController@invoice')->name('orders.invoice');
    Route::put('/orders/{order}/status', 'Admin\OrderController@status')->name('orders.status');

    Route::resource('users', 'Admin\UserController')->only([
        'index', 'show', 'edit', 'update'
    ]);
});
