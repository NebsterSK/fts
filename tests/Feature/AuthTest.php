<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase {
    public function testRegisterTest() {
        $response = $this->get(route('register'));

        $response->assertStatus(200);
    }

    public function testLoginTest() {
        $response = $this->get(route('login'));

        $response->assertStatus(200);
    }

    public function testForgotPasswordTest() {
        $response = $this->get(route('password.request'));

        $response->assertStatus(200);
    }
}
