<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Order;

class CartTest extends TestCase {
    use RefreshDatabase;

    public function testIndexTest() {
        $response = $this->get(route('cart.index'));

        $response->assertStatus(200);
    }

    public function testAddTest() {
        $this->seed(\ProductsTableSeeder::class);

        $response = $this->post(route('cart.add'), [
            'id' => 1,
            'quantity' => 1,
            'color' => 'white',
            'size' => 'm',
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        Cart::destroy();
    }

    public function testRemoveTest() {
        Cart::add(1, 'T-shirt OLD', 1, 13, 175, [
            'color' => 'white',
            'size' => 'm'
        ]);

        $response = $this->get(route('cart.remove', ['id' => Cart::content()->first()->rowId]));

        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
        Cart::destroy();
    }
}
