<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Order;
use Gloudemans\Shoppingcart\Facades\Cart;

class OrdersTest extends TestCase {
    use RefreshDatabase;

    public function testCreateTest() {
        $response = $this->get(route('orders.create'));

        $response->assertStatus(200);
    }

//    public function testStoreTest() {
//        Cart::add(1, 'T-shirt OLD', 1, 13, 175, [
//            'color' => 'white',
//            'size' => 'm'
//        ]);
//
//        $response = $this->post(route('orders.store'), [
//            'name' => 'test',
//            'surname' => 'test',
//            'email' => 'test@gmail.com',
//            'phone' => 'test',
//            'street' => 'test',
//            'city' => 'test',
//            'zip' => 'test',
//            'country' => 'test',
//            'tac' => 1
//        ]);
//
//        $response->assertStatus(302);
//        $response->assertSessionHasNoErrors();
//    }

//    public function testPaymentTest() {
//        $order = factory(Order::class)->create();
//
//        $response = $this->get(route('orders.payment', ['order' => $order]));
//
//        $response->assertStatus(200);
//    }
}
