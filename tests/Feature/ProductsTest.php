<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductsTest extends TestCase {
    public function testIndexTest() {
        $response = $this->get(route('products.index'));

        $response->assertStatus(200);
    }

    public function testOldTest() {
        $sizes = ['s', 'm', 'l'];

        foreach ($sizes as $size) {
            $response = $this->get(route('products.old', ['color' => 'white', 'size' => $size]));

            $response->assertStatus(200);
        }
    }

    public function testSoberTest() {
        $sizes = ['s', 'm', 'l'];

        foreach ($sizes as $size) {
            $response = $this->get(route('products.sober', ['color' => 'black', 'size' => $size]));

            $response->assertStatus(200);
        }
    }
}
