<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PublicAreaTest extends TestCase {
    public function testIndexTest() {
        $response = $this->get(route('index'));

        $response->assertStatus(200);
    }

    public function testBrandTest() {
        $response = $this->get(route('brand'));

        $response->assertStatus(200);
    }

    public function testContactTest() {
        $response = $this->get(route('contact'));

        $response->assertStatus(200);
    }

    public function testFaqTest() {
        $response = $this->get(route('faq'));

        $response->assertStatus(200);
    }

    public function testPpTest() {
        $response = $this->get(route('pp'));

        $response->assertStatus(200);
    }

    public function testTacTest() {
        $response = $this->get(route('tac'));

        $response->assertStatus(200);
    }

    public function testCarTest() {
        $response = $this->get(route('car'));

        $response->assertStatus(200);
    }
}
